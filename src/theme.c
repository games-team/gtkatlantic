/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gtk/gtk.h>

#include <dirent.h>

#include "global.h"
#include "theme.h"
#include "parser.h"
#include "interface.h"
#include "callback.h"


/* refresh themes database */
void theme_build_database()   {

	DIR *d;
	struct dirent *dir;
	gchar *path, *path2;
	guint32 i;

	/* first free all the database */
	for(i = 0; i < MAX_THEMES; i++)  {

		if(!data->theme[i].open)  continue;
		theme_del_entry(i);
	}

	/* user home theme */
	if (global->path_home) {
		path = g_strconcat(global->path_home, "/themes", NULL);
		d = opendir (path);

		if(d)  {

			while (1) {

				dir = readdir(d);
				if(!dir) break;

				if(dir->d_name[0] == '.')  continue;

				path2 = g_strconcat(path, "/", dir->d_name, "/", NULL);
				theme_add_entry(path2);
				g_free(path2);
			}
			closedir (d);
		}
		g_free(path);
	}

	/* global theme */
	d = opendir (PACKAGE_DATA_DIR "/themes");
	if (d) {

		while (1) {

			dir = readdir(d);
			if(!dir) break;

			if(dir->d_name[0] == '.')  continue;

			path2 = g_strconcat(PACKAGE_DATA_DIR "/themes/", dir->d_name, "/", NULL);
			theme_add_entry(path2);
			g_free(path2);
		}
		closedir (d);
	}
}


gboolean theme_get_valid_slot(guint32 *themeslot)  {

	guint32 i;

	if(!data) return(FALSE);

	for(i = 0; i < MAX_THEMES; i++)
		if(!data->theme[i].open) {

			*themeslot = i;
			return(TRUE);
		}

	return(FALSE);
}


void theme_del_entry(guint32 slot) {

	if(!data->theme[slot].open) return;

	g_free(data->theme[slot].path);
	g_free(data->theme[slot].path_preview);
	g_free(data->theme[slot].path_conf);

	g_free(data->theme[slot].name);
	g_free(data->theme[slot].type);
	g_free(data->theme[slot].version);
	g_free(data->theme[slot].author);
	g_free(data->theme[slot].email);

	data->theme[slot].open = FALSE;
}


void theme_add_entry(gchar *path)  {

	guint32 slot, area = 0;
	gchar *path2, str[512], *ident;
	struct stat s;
	FILE *file;
	char *ret;

	gchar *name = 0, *type = 0, *version = 0, *author = 0, *email = 0, *preview = 0;

	/* get properties of this theme, name, author, preview file, ... */
	path2 = g_strconcat(path, "theme.conf", NULL);

	/* detect invalid theme */
	if(stat(path2, &s) < 0)  {

		g_free(path2);
		return;
	}

	/* parse general group */
	file = fopen(path2, "r");
	if (!file) return;

	for (ret = fgets(str, 512, file); ret && !feof(file); ret = fgets(str, 512, file)) {

		if (str[0] == '[')  {

			if(! strncmp(str, "[general]", 9) )  area = THEME_FILE_SECTION_GENERAL;
			else area = 0;
		}

		else  if(area == THEME_FILE_SECTION_GENERAL)  {

			ident = parser_get_identifier(str);
			if(!ident)  continue;

			if(! strcmp(ident, "theme") )  {

				name = parser_get_data(str, "name");
				type = parser_get_data(str, "type");
				version = parser_get_data(str, "version");

			} else if(! strcmp(ident, "author") )  {

				author = parser_get_data(str, "name");
				email = parser_get_data(str, "email");

			} else if(! strcmp(ident, "misc") )  {

				preview = parser_get_data(str, "preview");
			}

			g_free(ident);
		}
	}
	fclose(file);


	/* giving up if theme provide no name or no type */
	if(!name  ||  !type)  return;


	/* fill the theme database */
	if(! theme_get_valid_slot(&slot) )  return;

	data->theme[slot].path = g_strdup(path);
	data->theme[slot].path_preview = g_strconcat(path, preview, NULL);
	g_free(preview);
	data->theme[slot].path_conf = g_strconcat(path, "theme.conf", NULL);

	data->theme[slot].open = TRUE;
	data->theme[slot].name = name;
	data->theme[slot].type = type;
	data->theme[slot].version = version;
	data->theme[slot].author = author;
	data->theme[slot].email = email;
}


void theme_build_selection_win()  {

	GtkWidget *ThemeWin;
		GtkWidget *VBox;
			GtkWidget *HBox;
				GtkWidget *VBoxLeft;
					GtkWidget *ScrollThemeList;
						GtkWidget *ThemeCList;
					GtkWidget *HBoxButtons;
						GtkWidget *Button;
				GtkWidget *VBoxInfo;
					GtkWidget *GridInfo;
						GtkWidget *Label;
				GtkWidget *PreviewFrame;

	GtkWidget *tmp;
	gchar *str;
	GtkTreeSelection *select;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkListStore *store;

	if(global->ThemeWin)  return;

	global->ThemeWin = ThemeWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(ThemeWin), "GtkAtlantic: Theme Selection");
	g_signal_connect(G_OBJECT(ThemeWin), "delete_event", G_CALLBACK(CallBack_ThemeWin_Delete), NULL);
	g_signal_connect(G_OBJECT(ThemeWin), "destroy", G_CALLBACK(CallBack_ThemeWin_Destroyed), NULL);

	VBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(VBox), 5);
	gtk_container_add(GTK_CONTAINER(ThemeWin), VBox);

	HBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(VBox), HBox, TRUE, TRUE, 0);

	VBoxLeft = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_box_pack_start(GTK_BOX(HBox), VBoxLeft, TRUE, TRUE, 0);

	/* theme list */
	ScrollThemeList = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_size_request(ScrollThemeList, 140, -1);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ScrollThemeList), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(VBoxLeft), ScrollThemeList, TRUE, TRUE, 0);

	ThemeCList = gtk_tree_view_new();
	g_object_set_data(G_OBJECT(ThemeWin), "theme_list", ThemeCList);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(ThemeCList), FALSE);
	gtk_container_add(GTK_CONTAINER(ScrollThemeList), ThemeCList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(ThemeCList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(Callback_ThemeList_Select), NULL);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Theme", renderer, "text", THEMELIST_COLUMN_NAME, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ThemeCList), column);

	store = gtk_list_store_new(TRADEPLAYERLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_INT);
	gtk_tree_view_set_model(GTK_TREE_VIEW(ThemeCList), GTK_TREE_MODEL(store));
	g_object_unref(store);

	/* hbox for buttons */
	HBoxButtons = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(VBoxLeft), HBoxButtons, FALSE, FALSE, 0);

	Button = gtk_button_new_with_label("Apply");
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_Theme_Apply), NULL);
	gtk_box_pack_start(GTK_BOX(HBoxButtons), Button, TRUE, TRUE, 0);

	Button = gtk_button_new_with_label("Cancel");
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_ThemeWin_Delete), NULL);
	gtk_box_pack_start(GTK_BOX(HBoxButtons), Button, TRUE, TRUE, 0);

	/* vbox for themes */
	VBoxInfo = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_box_pack_start(GTK_BOX(HBox), VBoxInfo, TRUE, TRUE, 0);

	/* table which contain theme infos */
	GridInfo = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(GridInfo), 5);
	gtk_grid_set_row_spacing(GTK_GRID(GridInfo), 2);
	gtk_box_pack_start(GTK_BOX(VBoxInfo), GridInfo, FALSE, FALSE, 0);

	Label = gtk_label_new("Name:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 0, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_name", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 0, 1, 1);

	Label = gtk_label_new("Type:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 1, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_type", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 1, 1, 1);

	Label = gtk_label_new("Version:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 2, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_version", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 2, 1, 1);

	Label = gtk_label_new("Author:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 3, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_author", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 3, 1, 1);

	Label = gtk_label_new("Email:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 4, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_email", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 4, 1, 1);

	Label = gtk_label_new("Location:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 0, 5, 1, 1);

	Label = gtk_label_new("");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_location", Label);
	gtk_grid_attach(GTK_GRID(GridInfo), Label, 1, 5, 1, 1);

	/* preview */
	PreviewFrame = gtk_frame_new("Preview");
	gtk_box_pack_start(GTK_BOX(VBoxInfo), PreviewFrame, TRUE, TRUE, 0);
	gtk_widget_set_size_request(PreviewFrame, 400, 245);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_preview_container", PreviewFrame);

	tmp = gtk_label_new("No preview available");
	gtk_widget_set_halign(tmp, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(tmp, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(ThemeWin), "theme_preview_data", tmp);
	gtk_container_add(GTK_CONTAINER(PreviewFrame), tmp);

	str = g_strdup_printf("%d", -1);
	g_object_set_data_full(G_OBJECT(global->ThemeWin), "theme_preview_selected", str, g_free);

	gtk_widget_show_all(ThemeWin);
}


void theme_fill_selection_list()  {

	guint32 i;
	GtkWidget *ThemeList;
	GtkListStore *store;

	ThemeList = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_list");

	/* build theme list */
	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ThemeList)));
	gtk_list_store_clear(store);

	for(i = 0 ; i < MAX_THEMES ; i++)  {
		GtkTreeIter iter;

		if (!data->theme[i].open) {
			continue;
		}

		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
		  THEMELIST_COLUMN_NAME, data->theme[i].name,
		  THEMELIST_COLUMN_ID, i,
		  -1);
	}
}


void theme_preview_free()  {
	GtkWidget *Preview;

	/* free all preview data */
	Preview = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_preview_data");
	gtk_widget_destroy(Preview);
	g_object_set_data(G_OBJECT(global->ThemeWin), "theme_preview_data", NULL);
}


void theme_display(gint32 themeid)  {

	GtkWidget *Label;
	GtkWidget *Frame;
	GtkWidget *Preview;
	GdkPixbuf *pixbuf = NULL;
	gchar *tmp;

	if(! data->theme[themeid].open)  return;

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_name");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].name);

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_type");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].type);

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_version");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].version);

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_author");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].author);

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_email");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].email);

	Label = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_location");
	gtk_label_set_text(GTK_LABEL(Label), data->theme[themeid].path);

	tmp = g_strdup_printf("%d", themeid);
	g_object_set_data_full(G_OBJECT(global->ThemeWin), "theme_preview_selected", tmp, g_free);

	/* free all preview data */
	theme_preview_free();

	Frame = g_object_get_data(G_OBJECT(global->ThemeWin), "theme_preview_container");

	/* put new preview data */
	if(! (pixbuf = theme_preview_read(themeid)) )  {

		Preview = gtk_label_new("No preview available");
		g_object_set_data(G_OBJECT(global->ThemeWin), "theme_preview_data", Preview);
		gtk_container_add(GTK_CONTAINER(Frame), Preview);
		gtk_widget_show_all(Frame);
		return;
	}

	/* put preview image */
	Preview = gtk_image_new_from_pixbuf(pixbuf);
	gtk_widget_set_halign(Preview, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(Preview, GTK_ALIGN_CENTER);
	g_object_set_data(G_OBJECT(global->ThemeWin), "theme_preview_data", Preview);
	gtk_container_add(GTK_CONTAINER(Frame), Preview);
	gtk_widget_show_all(Frame);
	g_object_unref(pixbuf);
}


GdkPixbuf *theme_preview_read(gint32 themeid)  {
	gchar *path = data->theme[themeid].path_preview;
	GdkPixbuf *pixbuf = NULL;
	GError *err = NULL;

	if (!data->theme[themeid].open)  return NULL;
	if (path == NULL)  return NULL;

	/* load preview image */
	pixbuf = gdk_pixbuf_new_from_file(path, &err);
	if (!pixbuf) {
		fprintf(stderr, "error while loading %s: %s\n", path, err ? err->message : "(unknown)");
		g_error_free(err);
	}
	return pixbuf;
}


void theme_load(gint32 themeid)  {

	printf("%d\n", themeid);
	return;
}
