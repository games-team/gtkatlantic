/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef ENG_FRAME_H
#define ENG_FRAME_H

#include <glib.h>

/* -- prototypes for eng_frame.c -- */

void eng_create_frame_all();
void eng_create_frame(eng_frame *f);
eng_coord* eng_pic_get_zone_old(eng_obj *o);
eng_coord* eng_pic_get_zone_new(eng_obj *o);

void eng_update_bufout(eng_frame *f, eng_coord *zone, eng_list *obj_show);
void eng_clear_bufout(eng_frame *f, eng_coord *zone);

gboolean eng_zone_is_modified(eng_frame *f, eng_coord *zone);
guchar *eng_get_zone(eng_frame *f, eng_coord *zone);
eng_zone  *eng_get_next_zone(eng_frame *f);

/* -- end of prototypes -- */

#endif /* ENG_FRAME_H */
