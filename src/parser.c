/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <gtk/gtk.h>

#include "parser.h"

/* get identifier in a string to parse, identifier is the first word */
gchar *parser_get_identifier(gchar *str)  {

	gint32 i, len;
	gchar *str2 = NULL;

	len = strlen(str);

	for(i = 0 ; i < len ; i++)  {

		if(str[i] == ' ')  {

			str2 = g_strndup(str, i);
			break;
		}
	}

	return(str2);
}

/* get a data from a key in a string to parse */
gchar *parser_get_data(gchar *str, gchar *key)  {

	gint32 i, len, len_key2, start = 0;
	gchar *key2, *str2 = NULL;
	gboolean get_data = 0;

	len = strlen(str);

	key2 = g_strconcat(key, "=\"", NULL);
	len_key2 = strlen(key2);

	for(i = 0 ; i < len ; i++)  {

		if(!get_data)  {

			if(! strncmp(str + i, key2, len_key2) )  {

				get_data = TRUE;
				i += len_key2 -1;
				start = i +1;
			}
		}
		else {

			if(str[i] == '"')  {

				str2 = g_strndup(str + start, i - start);
				break;
			}
		}
	}

	g_free(key2);
	return str2;
}
