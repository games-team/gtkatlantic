/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <glib.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "eng_list.h"


eng_list *eng_list_new()  {

	eng_list *l;
	l = g_malloc0( sizeof(eng_list) );
	return l;
}


eng_list *eng_list_destroy(eng_list *l) {

	if(!l)  return NULL;
	eng_list_clean(l);
	g_free(l);
	return NULL;
}


eng_list_e *eng_list_element_new(eng_list *l, void *d)  {

	eng_list_e *le;

	if(!l)  return NULL;
	le = g_malloc0( sizeof(eng_list_e) );
	if(!l->length)  l->first = l->last = le;

	le->list = l;
	le->data = d;
	l->length++;

	return le;
}


eng_list_e *eng_list_append(eng_list *l, void *d)  {

	eng_list_e *le;

	le = eng_list_element_new(l, d);

	if(l->last != le)  {
		le->prev = l->last;
		le->prev->next = le;
		l->last = le;
	}
	else  le->prev = NULL;
	le->next = NULL;

	return le;
}


eng_list_e *eng_list_prepend(eng_list *l, void *d)  {

	eng_list_e *le;

	le = eng_list_element_new(l, d);

	if(l->first != le)  {
		le->next = l->first;
		le->next->prev = le;
		l->first = le;
	}
	else  le->next = NULL;
	le->prev = NULL;

	return le;
}


void eng_list_remove_fast(eng_list_e *le)  {

	eng_list *l;

	if(!le)  return;
	l = le->list;
	l->length--;

	if(!le->prev) {
		if(le->next)  {  // prev = 0, next = 1
			l->first = le->next;
			le->next->prev = NULL;
		}
		else /* if(!le->next) */ {  // prev = 0, next = 0
			l->first = l->last = NULL;
		}
	}
	else /* if(le->prev) */ {  // prev = 1, next = 0
		if(!le->next) {
			l->last = le->prev;
			le->prev->next = NULL;
		}
		else /* if(le->next) */ {  // prev = 1, next = 1
			le->next->prev = le->prev;
			le->prev->next = le->next;
		}
	}

	g_free(le);
}


void eng_list_remove(eng_list *l, void *d)  {

	eng_list_e *le;
	if(!l)  return;

	if( (le = eng_list_find(l, d)) )
		eng_list_remove_fast(le);
}


eng_list_e *eng_list_first(eng_list *l)  {

	if(!l)  return NULL;
	return(l->first);
}


eng_list_e *eng_list_last(eng_list *l)  {

	if(!l)  return NULL;
	return(l->last);
}


eng_list_e *eng_list_previous(eng_list_e *le)  {

	if(!le)  return NULL;
	return(le->prev);
}


eng_list_e *eng_list_next(eng_list_e *le)  {

	if(!le)  return NULL;
	return(le->next);
}


void eng_list_clean(eng_list *l)  {

	if(!l)  return;
	while(l->first)  eng_list_remove_fast(l->first);
}


guint32 eng_list_length(eng_list *l)  {

	if(!l)  return 0;
	return(l->length);
}


eng_list_e *eng_list_find(eng_list *l, void *d)  {

	eng_list_e *le;
	if(!l)  return NULL;
	for(le = l->first ; le ; le = le->next)
		if(le->data == d)  {
			return le;
		}
	return NULL;
}


GList *eng_list_libevlist_to_glist(eng_list *l)  {

	eng_list_e *le;
	GList *gl = NULL;

	for(le = eng_list_first(l); le; le = le->next)
		gl = g_list_append(gl, le->data);

	return g_list_first(gl);
}


eng_list *eng_list_glist_to_libevlist(GList *gl)  {

	eng_list *l;
	GList *gle;

	l = eng_list_new();

	for(gle = g_list_first(gl); gle; gle = gle->next)
		eng_list_append(l, gle->data);

	return l;
}
