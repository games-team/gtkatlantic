/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBAL_H
#define GLOBAL_H

/* ** contain global variables */
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <time.h>
#include <sys/time.h>

#include "engine.h"

#ifndef WIN32

typedef int socket_t;

#else /* WIN32 */

#include <winsock2.h>
typedef SOCKET socket_t;
#undef PACKAGE_DATA_DIR
#undef PACKAGE_ICON_DIR
#define PACKAGE_DATA_DIR "./data"
#define PACKAGE_ICON_DIR "./share/icons/hicolor"

#endif /* WIN32 */

/* -- hierarchy
 *
 *  player[]                 > player game data
 *  game[]                   > game data
 *    |-> estate[]           > estate game data
 *    |-> group[]            > group game data
 *    |-> card[]             > card removed from stack (needed for trade)
 *    |-> trade[]            > current trades data
 *    `-> command[]          > command game data
 *  data                     > interface datas
 *    |-> estate[]           > interface estates data (tablepos, sizes, ...)
 *    |-> playerlist_card[]  > interface playerlist cards data (tablepos, sizes, ...)
 *    `-> theme[]            > themes database
 *  global                   > main data
 *    `-> connection         > connections status
 *  config                   > config data
 */

/* -- game data */
#define MAX_ESTATES 50
#define MAX_GROUPS 50
#define MAX_CARDS 16
#define MAX_TRADES 64
#define MAX_COMMANDS 20
#define MAX_PLAYERLIST_CARDS 30

/* -- player data */
#define PLAYER_ACTION_TRADE 0
#define PLAYER_ACTION_VERSION 1
#define PLAYER_ACTION_DATE 2
#define PLAYER_ACTION_PING 3
struct player_ {
	struct player_ *next;

	guint32  playerid;
	gint32   game;
	gchar   *image;
	gchar   *name;
	gchar   *host;
	gint32   money;
	guint16  location;
	gboolean jailed;
	gboolean directmove;
	gboolean bankrupt;
	gboolean hasturn;
	gboolean can_roll;
	gboolean can_buyestate;
	gboolean spectator;
	gint32   turnorder;

	guint16 location_to;

	/* playerlist */
	GtkWidget *playerlist_box;

	GtkWidget *playerlist_LabelNamePlayer;
	GtkWidget *playerlist_LabelMoneyPlayer;

	GtkWidget *playerlist_cards_gdkrgb;
	eng_frame *playerlist_cards_frame;
	eng_obj   *playerlist_cards_pic[MAX_PLAYERLIST_CARDS];

	GtkWidget *playerlist_token_gdkrgb;
	eng_frame *playerlist_token_frame;
	eng_obj   *playerlist_token_pic;

	/* board */
	gint32 tokenid;
	eng_obj *token_pic;
};
typedef struct player_ player;

extern player *player_h;  /* Player list */

/* -- estate data */
#define ESTATE_ACTION_BUILDHOUSE 0
#define ESTATE_ACTION_SELLHOUSE 1
#define ESTATE_ACTION_MORTGAGE 2
#define ESTATE_ACTION_SELL 3
typedef struct {

	gchar   *name;
	guint8   color[3];   // R, G, B
	guint8   bgcolor[3]; // R, G, B
	gint32   owner;
	guint8   houses;
	guint32  houseprice;
	guint32  sellhouseprice;
	gint32   mortgageprice;
	gint32   unmortgageprice;
	gboolean mortgaged;
	gint16   group;
	gboolean can_be_owned;
	gboolean can_toggle_mortgage;
	gboolean can_buy_houses;
	gboolean can_sell_houses;
	guint32  money;
	guint32  price;
	guint32  rent[6];

	/* board */
	GtkWidget *gdkrgb;
	eng_obj   *pic;
	eng_obj   *star_pic;
	eng_obj   *house_pic;

} _estate;


/* -- group data */
typedef struct {

	gchar *name;

} _group;


/* -- card data */
typedef struct {

	gint32 cardid;
	gint32 owner;
	gchar *title;

} _card;


#define TRADE_MAX_PLAYER 9

#define TRADE_ACTION_REJECT 0
#define TRADE_ACTION_ACCEPT 1
#define TRADE_ACTION_REMOVE 2

#define TRADE_TYPE_NONE 0
#define TRADE_TYPE_MONEY 1
#define TRADE_TYPE_ESTATE 2
#define TRADE_TYPE_CARD 3
enum {
	TRADEPROPOSALLIST_COLUMN_TYPE,
	TRADEPROPOSALLIST_COLUMN_FROM,
	TRADEPROPOSALLIST_COLUMN_GIVES,
	TRADEPROPOSALLIST_COLUMN_TO,
	TRADEPROPOSALLIST_COLUMN_WHAT,
	TRADEPROPOSALLIST_COLUMN_TYPE_ID,
	TRADEPROPOSALLIST_COLUMN_FROM_ID,
	TRADEPROPOSALLIST_COLUMN_TO_ID,
	TRADEPROPOSALLIST_COLUMN_AUX_ID,
	TRADEPROPOSALLIST_COLUMN_NUM
};
enum {
	TRADEPLAYERLIST_COLUMN_NAME,
	TRADEPLAYERLIST_BGCOLOR_NAME,
	TRADEPLAYERLIST_COLUMN_NUM
};
/* -- trade data */
struct trade_player_ {
	player *player;
	gboolean accept;
};
typedef struct trade_player_ trade_player;

typedef struct {

	gboolean open;

	gint32 tradeid;
	gint32 revision;
	trade_player player[TRADE_MAX_PLAYER];
	guint8 current_component;

	GtkWidget *TradeWin;
	GtkWidget *PlayerList;
	GtkWidget *FrameComponent;
	GtkWidget *ProposalList;
	GtkWidget *ComponentBox;
	GtkWidget *SubComponentBox;

	guint8  select_type;  // money, estate, card
	guint32 select_from;
	guint32 select_to;
	guint32 select_aux;   // estateid, cardid

} _trade;


/* -- command data */
typedef struct {

	gboolean open;

	GtkWidget *Frame;
	GtkWidget *gdkrgb;
	gboolean   redraw;

	gchar *command;

} _command;


#define GAME_STATUS_NONE 0
#define GAME_STATUS_CONFIG 1
#define GAME_STATUS_INIT 2
#define GAME_STATUS_RUN 3
#define GAME_STATUS_END 4
enum {
	PLAYERLIST_COLUMN_NAME,
	PLAYERLIST_COLUMN_HOST,
	PLAYERLIST_COLUMN_NUM
};
enum {
	AUCTIONPLAYERLIST_COLUMN_NAME,
	AUCTIONPLAYERLIST_COLUMN_BID,
	AUCTIONPLAYERLIST_COLUMN_PLAYERID,
	AUCTIONPLAYERLIST_WEIGHT,
	AUCTIONPLAYERLIST_WEIGHTSET,
	AUCTIONPLAYERLIST_COLUMN_NUM
};

struct game_ {
	struct game_ *next;

	guint8 status;
	gint32 gameid;
	gint32 master;

	_estate estate[MAX_ESTATES];
	_group group[MAX_GROUPS];
	_card card[MAX_CARDS];
	_trade trade[MAX_TRADES];
	_command command[MAX_COMMANDS];

	time_t start_time;
	eng_frame *board_frame;

	GtkWidget *PlayerList;

	GtkWidget *ChatBox, *Chat;

	GtkWidget *GameConfigBox;
	GtkWidget *BoardCenter;
	GtkWidget *WinEstateTree;

	guint32 timeout_token;

};
typedef struct game_ game;

extern game *currentgame;  /* Currently played game entry */
extern game *game_h;  /* Game list */


/* -- connect data */
#define CONNECT_TYPE_MONOPD_GETGAME 1
#define CONNECT_TYPE_MONOPD_GAME 2
#define CONNECT_TYPE_METASERVER 3
struct connection_ {
#if DEBUG
	guint32 id;
#endif /* DEBUG */

	guint8   type;

	gchar   *host;
	gchar   *ip;
	gint32  port;
	socket_t socket;
	GIOChannel *channel;
	gint   event_source_id;

	gchar  *server_version;

	gchar *buffer_in;
	gchar *buffer_out;

};
typedef struct connection_ connection;


/* interface card */
typedef struct  {

	guint16 x;
	guint16 y;
	guint8  z;

	guint8  buffer_card;
	guint8  estateid;

} _interface_playerlist_card;


/* interface estate */
#define TYPE_HOUSE_NONE 0
#define TYPE_HOUSE_HORIZONTAL 1
#define TYPE_HOUSE_VERTICAL 2
typedef struct  {

	guint16 x;
	guint16 y;

	guint8  buffer_board;

	guint16 x1token;
	guint16 y1token;
	guint16 x2token;
	guint16 y2token;

	guint16 x1jail;
	guint16 y1jail;
	guint16 x2jail;
	guint16 y2jail;

	guint16 xstar;
	guint16 ystar;

	guint16 xhouse;
	guint16 yhouse;
	guint8  type_house;

} _interface_estate;


/* themes database */
enum {
	THEMELIST_COLUMN_NAME,
	THEMELIST_COLUMN_ID,
	THEMELIST_COLUMN_NUM
};
typedef struct {

	gboolean open;

	gchar *path;
	gchar *path_preview;
	gchar *path_conf;

	gchar *name;
	gchar *type;
	gchar *version;
	gchar *author;
	gchar *email;

} _theme;


/* interface / pngfiles */
#define MAX_PLAYERS 20
#define MAX_COMMANDS 20
#define MAX_HOUSES 10
#define MAX_THEMES 256
typedef struct  {

	/* pngfile board */
	gchar  *pngfile_board_filename;
	guint16  pngfile_board_x[MAX_ESTATES],     pngfile_board_y[MAX_ESTATES];
	guint16  pngfile_board_width[MAX_ESTATES], pngfile_board_height[MAX_ESTATES];
	GdkPixbuf *pngfile_board_buf[MAX_ESTATES];

	/* pngfile_tokens */
	gchar  *pngfile_token_filename;
	guint16  pngfile_token_x[MAX_PLAYERS],      pngfile_token_y[MAX_PLAYERS];
	guint16  pngfile_token_width[MAX_PLAYERS],  pngfile_token_height[MAX_PLAYERS];
	GdkPixbuf *pngfile_token_buf[MAX_PLAYERS];

	/* pngfile_stars */
	gchar  *pngfile_star_filename;
	guint16  pngfile_star_x[MAX_PLAYERS],      pngfile_star_y[MAX_PLAYERS];
	guint16  pngfile_star_width[MAX_PLAYERS],  pngfile_star_height[MAX_PLAYERS];
	GdkPixbuf *pngfile_star_buf[MAX_PLAYERS];

	/* pngfile_stars_m (mortgaged) */
	gchar  *pngfile_star_m_filename;
	guint16  pngfile_star_m_x[MAX_PLAYERS],      pngfile_star_m_y[MAX_PLAYERS];
	guint16  pngfile_star_m_width[MAX_PLAYERS],  pngfile_star_m_height[MAX_PLAYERS];
	GdkPixbuf *pngfile_star_m_buf[MAX_PLAYERS];

	/* pngfile_cards */
	gchar  *pngfile_card_filename;
	guint16  pngfile_card_x[MAX_PLAYERLIST_CARDS],     pngfile_card_y[MAX_PLAYERLIST_CARDS];
	guint16  pngfile_card_width[MAX_PLAYERLIST_CARDS], pngfile_card_height[MAX_PLAYERLIST_CARDS];
	GdkPixbuf *pngfile_card_buf[MAX_PLAYERLIST_CARDS];

	/* pngfile_commands */
	gchar  *pngfile_command_filename;
	guint16  pngfile_command_x[MAX_COMMANDS],      pngfile_command_y[MAX_COMMANDS];
	guint16  pngfile_command_width[MAX_COMMANDS],  pngfile_command_height[MAX_COMMANDS];
	GdkPixbuf *pngfile_command_buf[MAX_COMMANDS];

	/* pngfile_horiz_houses */
	gchar  *pngfile_horiz_house_filename;
	guint16  pngfile_horiz_house_x[MAX_HOUSES],      pngfile_horiz_house_y[MAX_HOUSES];
	guint16  pngfile_horiz_house_width[MAX_HOUSES],  pngfile_horiz_house_height[MAX_HOUSES];
	GdkPixbuf *pngfile_horiz_house_buf[MAX_HOUSES];

	/* pngfile_vert_houses */
	gchar  *pngfile_vert_house_filename;
	guint16  pngfile_vert_house_x[MAX_HOUSES],      pngfile_vert_house_y[MAX_HOUSES];
	guint16  pngfile_vert_house_width[MAX_HOUSES],  pngfile_vert_house_height[MAX_HOUSES];
	GdkPixbuf *pngfile_vert_house_buf[MAX_HOUSES];


	/* load flags */
	gboolean png_game_loaded;


	/* interface estate */
	guint16 board_height, board_width;
	guint16 board_center_x, board_center_y, board_center_width, board_center_height;
	guint8  number_estates;
	_interface_estate estate[MAX_ESTATES];

	/* interface playerlist token */
	guint16 playerlist_token_width, playerlist_token_height;
	guint8 token_max;

	/* interface playerlist cards */
	guint16 playerlist_cards_width, playerlist_cards_height;
	guint8  playerlist_cards_alphaunowned, playerlist_cards_alphaowned, playerlist_cards_alphamortgage;
	gint32  playerlist_cards_cardbgcolor, playerlist_cards_cardbgcolormortgage;
	guint8  number_playerlist_card;
	_interface_playerlist_card playerlist_card[MAX_PLAYERLIST_CARDS];


	/* themes database */
	_theme theme[MAX_THEMES];

} _data;

extern _data *data; /* UI data  */


/* -- main data */
#define PHASE_GETGAMES   1
#define PHASE_GAMECREATE 2
#define PHASE_GAMEINIT   3
#define PHASE_GAMEPLAY   4
enum {
	GAMELIST_COLUMN_HOST,
	GAMELIST_COLUMN_VERSION,
	GAMELIST_COLUMN_GAME,
	GAMELIST_COLUMN_STATUS,
	GAMELIST_COLUMN_PLAYERS,
	GAMELIST_COLUMN_PORT,
	GAMELIST_COLUMN_GAMETYPE,
	GAMELIST_COLUMN_GAMEID,
	GAMELIST_COLUMN_SERVERID,
	GAMELIST_COLUMN_CANBEJOINED,
	GAMELIST_COLUMN_CANBEWATCHED,
	GAMELIST_COLUMN_BGCOLOR,
	GAMELIST_COLUMN_SOURCE,
	GAMELIST_COLUMN_NUM
};
enum {
	GAMELIST_SOURCE_METASERVER,
	GAMELIST_SOURCE_CUSTOM,
};
enum {
	SERVERLIST_COLUMN_HOST,
	SERVERLIST_COLUMN_PORT,
	SERVERLIST_COLUMN_VERSION,
	SERVERLIST_COLUMN_USERS,
	SERVERLIST_COLUMN_SERVERID,
	SERVERLIST_COLUMN_NUM
};
typedef struct  {

	gchar *path_home;

	gchar *bg_green;
	gchar *bg_red;

	guint8 phase;

	GtkWidget *MainWin;
	GtkWidget *MainBox;
	GtkWidget *Menu;
	GtkWidget *MainVerticalBox;
	GtkWidget *InfoLabel;
	guint32 timeout_InfoLabel;

	connection *customserver_connect;
	connection *metaserver_connect;
	connection *game_connect;

	GtkListStore *game_store;
	GtkListStore *server_store;
	GtkTreeIter selected_game;

	GtkWidget *ConfigWin;
	GtkWidget *HelpWin;
	GtkWidget *ThemeWin;

	gint32 my_playerid;
	gchar   *cookie;
} _global;

extern _global *global; /* Global state */


/* all configuration */
#define METASERVER_HOST "meta.atlanticd.net"
#define METASERVER_PORT 1240

#define GAME_PLAYERLIST_POS_LEFT  0
#define GAME_PLAYERLIST_POS_RIGHT 1
typedef struct  {

	gchar   *nickname;

	gboolean  metaserver_autoconnect;
	gboolean  metaserver_sendclientversion;

	gchar   *getgames_host;
	guint32   getgames_port;
	gboolean  getgames_autoconnect;

	guint32   chat_max_lines;

	guint8    game_playerlist_position;
	guint32   game_token_animation_speed;
	gboolean  game_token_transparency;

} _config;

extern _config *config; /* Configuration */

#endif /* GLOBAL_H */
