/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef ENG_MAIN_H
#define ENG_MAIN_H

/* ENG_MAIN: main functions for engine
 *
 *
 * all int functions return 0 if error occur
 * return >0 when sucess
 *
 * all functions kill process if error occurs when allocate memory
 */


/* ---- info about: x, y, z
 *
 *           o-------- x
 *          /|
 *         / |
 *        /  |
 *       /   |
 *      z    |
 *           y
 *
 * z = ground, 0 is background
 *
 */

/* ---- struct hierarchy
 *
 * eng                  > global engine variables
 *   `-> eng_alpha      > alpha tables
 * eng_frame            > property of frames (height, width, ...)
 *   |-> eng_zone_upd   > display zone to update (x1, y1, x2, y1)
 *   `-> eng_obj        > property of pictures (x, y, height, width, ...)
 */

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "eng_list.h"


#define MAX_HEIGHT 0xffff
#define MAX_WIDTH 0xffff

#define ENG_DEBUG FALSE


/* -- prototypes for eng_main.c -- */
typedef struct eng_coord_ eng_coord;
typedef struct eng_alpha_ eng_alpha;
typedef struct _eng_ _eng;
extern _eng *eng;
typedef struct eng_frame_ eng_frame;
typedef struct eng_obj_ eng_obj;
typedef struct eng_zone_ eng_zone;

void eng_init();
void eng_close();

eng_frame* eng_frame_create();
void eng_frame_destroy        (eng_frame *f);
void eng_frame_destroy_all();
void eng_frame_reset          (eng_frame *f);
void eng_frame_free_zoneupd   (eng_frame *f);
void eng_frame_set_compute    (eng_frame *f);
void eng_frame_unset_compute  (eng_frame *f);
void eng_frame_clean          (eng_frame *f);
void eng_frame_set_height     (eng_frame *f, guint16 height);
void eng_frame_set_width      (eng_frame *f, guint16 width);
void eng_frame_showarg        (eng_frame *f);

eng_obj* eng_pic_create       (eng_frame *f);
void eng_pic_destroy          (eng_obj *o);  //sent a call to destroy pic
void eng_pic_free             (eng_obj *o);  //real destruction, never use is directly
void eng_pic_reset            (eng_obj *o);
void eng_pic_show             (eng_obj *o);
void eng_pic_unshow           (eng_obj *o);
void eng_pic_redraw           (eng_obj *o);
void eng_pic_set_x            (eng_obj *o, guint16 x);
void eng_pic_set_y            (eng_obj *o, guint16 y);
void eng_pic_set_z            (eng_obj *o, guint16 z);
void eng_pic_set_height       (eng_obj *o, guint16 height);
void eng_pic_set_width        (eng_obj *o, guint16 width);
void eng_pic_set_alpha        (eng_obj *o, guint8 alpha);
void eng_pic_unset_alpha      (eng_obj *o);
void eng_pic_set_bgcolor      (eng_obj *o, guint32 bgcolor);
void eng_pic_unset_bgcolor    (eng_obj *o);
void eng_pic_set_pixbuf       (eng_obj *o, GdkPixbuf *pixbuff);
void eng_pic_showarg          (eng_obj *o);
gboolean eng_pic_test         (eng_obj *o);
/* -- end of prototypes -- */


struct eng_coord_ {

	gint32 x1;
	gint32 y1;
	gint32 x2;
	gint32 y2;

	gint32 x;
	gint32 y;
	gint32 width;
	gint32 height;
	gint32 stride;
	eng_list_e *entry;
};


struct _eng_ {

	eng_alpha *alpha;             // alpha tables
	eng_list *obj;
	eng_list *frame;

} ;


struct eng_alpha_ {

	guint8  fg[256][256]; // foreground [alpha][fg]
	guint8  bg[256][256]; // background [alpha][bg]

} ;


struct eng_frame_ {

	gboolean compute;     // compute/or not the frame

	guint16 width;        // set the width  of out buffer
	guint16 height;       // set the height of out buffer

	guchar * bufout;      // bufout contain image would you want to draw

// PRIVATE
	guint32 memalloc;    // how much mem is allocated for buf

	guint16 num_zone;     // number of zones to update
	guint16 x_min;       // min/max of zone to refresh
	guint16 y_min;
	guint16 x_max;
	guint16 y_max;

	eng_list *obj;
	eng_list *zone_upd;

	eng_list_e *entry;           // entry in frame list
};


struct eng_obj_ {

	eng_frame *frame;

	gboolean show;         // show/hide the pic
	gboolean destroy;      // destroy this picture

	guint16 x;       // x position
	guint16 y;       // y position
	guint16 z;       // z position (ground)

	guint16 width;   // width  of img
	guint16 height;  // height of img

	gboolean have_alpha;     // have an alpha
	gboolean have_bgcolor;   // have a  backgroundcolor

	guint8  alpha;     // alpha value
	guint8  bgcolor[3]; // background color

	GdkPixbuf *pixbuf;    // pixbuf contain image data

// PRIVATE
	gboolean change;      // if pic arg has changed

	guint16 x_old;        // x position old
	guint16 y_old;        // y position old

	guint16 width_old;    // width  of img old
	guint16 height_old;   // height of img old

	eng_list_e *entry;           // entry in frame object list
	eng_list_e *entry_main;      // entry in general object list
	eng_list_e *entry_valid;     // entry in tmp valid list
	eng_list_e *entry_show;      // entry in tmp show list
	eng_list_e *entry_modif;     // entry in tmp modif list
	eng_list_e *entry_zone;      // entry in zone modif list
};


struct eng_zone_ {

	gint32 x1;
	gint32 y1;
	gint32 x2;
	gint32 y2;
	gint32 height;
	gint32 width;
	guchar *aux;
};

#endif /* ENG_MAIN_H */
