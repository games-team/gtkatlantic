/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <gtk/gtk.h>

#include "global.h"
#include "trade.h"
#include "game.h"
#include "client.h"
#include "interface.h"
#include "callback.h"


void trade_initnew(gint32 tradeid)  {

	gint32 tradeslot;
	gchar *sendstr;

	/* already created */
	if(get_trade_slot_with_tradeid(tradeid) >= 0)  return;

	if(! game_get_valid_trade_slot(&tradeslot) )  {

		/* no trade slot valid -> reject trade */
		sendstr = g_strdup_printf(".Tr%d\n", tradeid);
		client_send(global->game_connect, sendstr);
		g_free(sendstr);
		return;
	}

	currentgame->trade[tradeslot].open = TRUE;
	currentgame->trade[tradeslot].tradeid = tradeid;

	trade_create_panel(tradeslot);
}


void trade_destroy(gint32 tradeid)  {

	gint32 tradeslot;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if(tradeslot < 0) return;

	trade_destroy_slot(tradeslot);
}


void trade_destroy_slot(gint32 tradeslot)  {

	if(!currentgame->trade[tradeslot].open) return;

	gtk_widget_destroy(currentgame->trade[tradeslot].TradeWin);
	memset(&currentgame->trade[tradeslot], 0, sizeof(_trade) );
}


void trade_create_panel(gint32 tradeslot)  {

	GtkWidget *TradeWin;
		GtkWidget *VBox;
			//components
		GtkWidget *HBox;
			GtkWidget *ScrollWinPlayer;
				GtkWidget *PlayerList;
			GtkWidget *VboxProposalButtons;
				GtkWidget *ScrollWinProposal;
					GtkWidget *ProposalList;
				GtkWidget *HBox_buttons;

	GtkWidget *Button;
	GtkTreeSelection *select;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkListStore *store;

	TradeWin = currentgame->trade[tradeslot].TradeWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(TradeWin, 600, 220);
	gtk_window_set_title(GTK_WINDOW(TradeWin), "GtkAtlantic: Trade");
	g_object_set_data(G_OBJECT(TradeWin), "command", GINT_TO_POINTER(TRADE_ACTION_REJECT));
	g_object_set_data(G_OBJECT(TradeWin), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	g_signal_connect(G_OBJECT(TradeWin), "delete_event", G_CALLBACK(CallBack_trade_button), NULL);

	/* component / hbox / buttons */
	VBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(TradeWin), VBox);

	/* component */
	currentgame->trade[tradeslot].FrameComponent = gtk_frame_new("Add Component");
	gtk_container_set_border_width(GTK_CONTAINER(currentgame->trade[tradeslot].FrameComponent), BORDER);
	gtk_box_pack_start(GTK_BOX(VBox), currentgame->trade[tradeslot].FrameComponent, FALSE, FALSE, 0);

	trade_rebuild_component(tradeslot);
	trade_rebuild_subcomponent(tradeslot);

	/* player / current proposal */
	HBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_container_set_border_width(GTK_CONTAINER(HBox), BORDER);
	gtk_box_pack_start(GTK_BOX(VBox), HBox, TRUE, TRUE, 0);

	/* player list */
	ScrollWinPlayer = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_size_request(ScrollWinPlayer, 130, -1);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ScrollWinPlayer), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(HBox), ScrollWinPlayer, FALSE, FALSE, 0);

	PlayerList = currentgame->trade[tradeslot].PlayerList = gtk_tree_view_new();
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(PlayerList), FALSE);
	gtk_container_add(GTK_CONTAINER(ScrollWinPlayer), PlayerList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(PlayerList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_NONE);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Player", renderer, "text", TRADEPLAYERLIST_COLUMN_NAME, "cell-background", TRADEPLAYERLIST_BGCOLOR_NAME, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(PlayerList), column);

	store = gtk_list_store_new(TRADEPLAYERLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model(GTK_TREE_VIEW(PlayerList), GTK_TREE_MODEL(store));
	g_object_unref(store);


	trade_rebuild_playerlist(tradeslot);
	trade_rebuild_subcomponent(tradeslot);


	/* vbox for proposal & buttons */
	VboxProposalButtons = gtk_box_new(GTK_ORIENTATION_VERTICAL, BORDER);
	gtk_box_pack_start(GTK_BOX(HBox), VboxProposalButtons, TRUE, TRUE, 0);

	/* current proposal */
	ScrollWinProposal = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ScrollWinProposal), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(VboxProposalButtons), ScrollWinProposal, TRUE, TRUE, 0);

	ProposalList = currentgame->trade[tradeslot].ProposalList = gtk_tree_view_new();
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(ProposalList), FALSE);
	gtk_container_add(GTK_CONTAINER(ScrollWinProposal), ProposalList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(ProposalList));
	g_object_set_data(G_OBJECT(select), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(Callback_ProposalList_Select), NULL);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Type", renderer, "text", TRADEPROPOSALLIST_COLUMN_TYPE, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ProposalList), column);
	column = gtk_tree_view_column_new_with_attributes("From", renderer, "text", TRADEPROPOSALLIST_COLUMN_FROM, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ProposalList), column);
	column = gtk_tree_view_column_new_with_attributes("Gives", renderer, "text", TRADEPROPOSALLIST_COLUMN_GIVES, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ProposalList), column);
	column = gtk_tree_view_column_new_with_attributes("To", renderer, "text", TRADEPROPOSALLIST_COLUMN_TO, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ProposalList), column);
	column = gtk_tree_view_column_new_with_attributes("What", renderer, "text", TRADEPROPOSALLIST_COLUMN_WHAT, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ProposalList), column);
 

	store = gtk_list_store_new(TRADEPROPOSALLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
	gtk_tree_view_set_model(GTK_TREE_VIEW(ProposalList), GTK_TREE_MODEL(store));
	g_object_unref(store);

	/* buttons */
	HBox_buttons = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	gtk_box_pack_end(GTK_BOX(VboxProposalButtons), HBox_buttons, FALSE, FALSE, 0);

	/* remove button */
	Button = gtk_button_new_with_label("Remove");
	g_object_set_data(G_OBJECT(Button), "command", GINT_TO_POINTER(TRADE_ACTION_REMOVE));
	g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_button), NULL);
	gtk_box_pack_start(GTK_BOX(HBox_buttons), Button, FALSE, FALSE, 0);

	/* accept button */
	Button = gtk_button_new_with_label("Accept");
	g_object_set_data(G_OBJECT(Button), "command", GINT_TO_POINTER(TRADE_ACTION_ACCEPT));
	g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_button), NULL);
	gtk_box_pack_end(GTK_BOX(HBox_buttons), Button, FALSE, FALSE, 0);

	/* reject button */
	Button = gtk_button_new_with_label("Reject");
	g_object_set_data(G_OBJECT(Button), "command", GINT_TO_POINTER(TRADE_ACTION_REJECT));
	g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_button), NULL);
	gtk_box_pack_end(GTK_BOX(HBox_buttons), Button, FALSE, FALSE, 0);

	gtk_widget_show_all(TradeWin);
}


void trade_rebuild_playerlist(gint32 tradeslot)  {

	GtkWidget *PlayerList;
	gint32 i;
	GtkListStore *store;

	if(!currentgame->trade[tradeslot].open)  return;

	PlayerList = currentgame->trade[tradeslot].PlayerList;
	if(!PlayerList) return;

	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(PlayerList)));
	gtk_list_store_clear(store);

	for (i = 0; i < TRADE_MAX_PLAYER; i++)  {
		trade_player *p = &currentgame->trade[tradeslot].player[i];
		GtkTreeIter iter;
		if (!p->player) continue;

		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
		  TRADEPLAYERLIST_COLUMN_NAME, p->player->name,
		  TRADEPLAYERLIST_BGCOLOR_NAME, p->accept ? global->bg_green : global->bg_red,
		  -1);
	}
}


void trade_rebuild_component(gint32 tradeslot)  {

	GtkWidget *box;
		GtkWidget *Combo;

	guint32 i;

	currentgame->trade[tradeslot].current_component = TRADE_TYPE_MONEY;

	if(currentgame->trade[tradeslot].ComponentBox)  {
		gtk_widget_destroy(currentgame->trade[tradeslot].ComponentBox);
		currentgame->trade[tradeslot].ComponentBox = 0;
		currentgame->trade[tradeslot].SubComponentBox = 0;
	}

	box = currentgame->trade[tradeslot].ComponentBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_container_set_border_width(GTK_CONTAINER(box), 5);
	gtk_container_add(GTK_CONTAINER(currentgame->trade[tradeslot].FrameComponent), currentgame->trade[tradeslot].ComponentBox);

	/* Choice component: Estate/Money/Card  */
	Combo = gtk_combo_box_text_new();
	g_object_set_data(G_OBJECT(Combo), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
	g_signal_connect(G_OBJECT(Combo), "changed", G_CALLBACK(CallBack_trade_sub_component), NULL);
	gtk_box_pack_start(GTK_BOX(box), Combo, FALSE, FALSE, 0);

	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), "Money");
	gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

	for(i = 0 ; i < data->number_estates ; i++)  {
		if(currentgame->estate[i].owner <= 0)  continue;

		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), "Estate");
		break;
	}

	for(i = 0 ; i < MAX_CARDS ; i++)  {
		if(!currentgame->card[i].owner)  continue;

		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), "Card");
		break;
	}

	gtk_widget_show_all(currentgame->trade[tradeslot].ComponentBox);
}


void trade_rebuild_subcomponent(gint32 tradeslot)  {

	GtkWidget *box;
		GtkWidget *Label;
		GtkWidget *Entry;
		GtkWidget *Combo;
		GtkWidget *Button;

	guint32 i;
	player *p;

	if(!currentgame->trade[tradeslot].ComponentBox) return;

	if(currentgame->trade[tradeslot].SubComponentBox)  {
		gtk_widget_destroy(currentgame->trade[tradeslot].SubComponentBox);
		currentgame->trade[tradeslot].SubComponentBox = 0;
	}

	box = currentgame->trade[tradeslot].SubComponentBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(currentgame->trade[tradeslot].ComponentBox), box, TRUE, TRUE, 0);

	switch(currentgame->trade[tradeslot].current_component)  {

		case TRADE_TYPE_ESTATE:

			/* update button */
			Button = gtk_button_new_with_label("Update");
			g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
			g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_update_component), NULL);
			gtk_widget_set_halign(Button, GTK_ALIGN_CENTER);
			gtk_widget_set_valign(Button, GTK_ALIGN_CENTER);
			gtk_box_pack_end(GTK_BOX(box), Button, FALSE, FALSE, 0);

			/* valid estates */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_estate", Combo);
			gtk_widget_set_hexpand(Combo, TRUE);
			gtk_box_pack_start(GTK_BOX(box), Combo, TRUE, TRUE, 0);

			for(i = 0 ; i < data->number_estates ; i++)  {

				if(currentgame->estate[i].owner <= 0)  continue;
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), currentgame->estate[i].name);
			}

			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			/* target label */
			Label = gtk_label_new("Target");
			gtk_box_pack_start(GTK_BOX(box), Label, FALSE, FALSE, 0);

			/* valid target players */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_target_player", Combo);
			gtk_box_pack_start(GTK_BOX(box), Combo, FALSE, FALSE, 0);

			for (p = player_h; p; p = p->next) {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), p->name);
			}

			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			break;


		case TRADE_TYPE_MONEY:

			/* update button */
			Button = gtk_button_new_with_label("Update");
			g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
			g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_update_component), NULL);
			gtk_widget_set_halign(Button, GTK_ALIGN_CENTER);
			gtk_widget_set_valign(Button, GTK_ALIGN_CENTER);
			gtk_box_pack_end(GTK_BOX(box), Button, FALSE, FALSE, 0);

			/* amount of money */
			Entry = gtk_entry_new();
			gtk_widget_set_hexpand(Entry, TRUE);
			g_object_set_data(G_OBJECT(Button), "amount_money", Entry);
			gtk_box_pack_start(GTK_BOX(box), Entry, TRUE, TRUE, 0);

			/* from label */
			Label = gtk_label_new("From");
			gtk_box_pack_start(GTK_BOX(box), Label, FALSE, FALSE, 0);

			/* valid from players */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_from_player", Combo);
			gtk_box_pack_start(GTK_BOX(box), Combo, FALSE, FALSE, 0);

			for (p = player_h; p; p = p->next)  {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), p->name);

			}

			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			/* to label */
			Label = gtk_label_new("To");
			gtk_box_pack_start(GTK_BOX(box), Label, FALSE, FALSE, 0);

			/* valid to players */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_to_player", Combo);
			gtk_box_pack_start(GTK_BOX(box), Combo, FALSE, FALSE, 0);

			for (p = player_h; p; p = p->next) {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), p->name);
			}
			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			break;


		case TRADE_TYPE_CARD:

			/* update button */
			Button = gtk_button_new_with_label("Update");
			g_object_set_data(G_OBJECT(Button), "tradeid", GINT_TO_POINTER(currentgame->trade[tradeslot].tradeid));
			g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_trade_update_component), NULL);
			gtk_widget_set_halign(Button, GTK_ALIGN_CENTER);
			gtk_widget_set_valign(Button, GTK_ALIGN_CENTER);
			gtk_box_pack_end(GTK_BOX(box), Button, FALSE, FALSE, 0);

			/* valid cards */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_card", Combo);
			gtk_widget_set_hexpand(Combo, TRUE);
			gtk_box_pack_start(GTK_BOX(box), Combo, TRUE, TRUE, 0);
			for(i = 0 ; i < MAX_CARDS ; i++)  {
				gchar *tmp;

				if(!currentgame->card[i].owner)  continue;
				tmp = g_strdup_printf("%d: %s", currentgame->card[i].cardid, currentgame->card[i].title);
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), tmp);
				g_free(tmp);
			}
			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			/* target label */
			Label = gtk_label_new("Target");
			gtk_box_pack_start(GTK_BOX(box), Label, FALSE, FALSE, 0);

			/* valid target players */
			Combo = gtk_combo_box_text_new();
			g_object_set_data(G_OBJECT(Button), "name_target_player", Combo);
			gtk_box_pack_start(GTK_BOX(box), Combo, FALSE, FALSE, 0);

			for (p = player_h; p; p = p->next) {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;
				gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), p->name);
			}
			gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);

			break;
	}

	gtk_widget_show_all(currentgame->trade[tradeslot].SubComponentBox);
}


void trade_update_revision(gint32 tradeid, gint32 revision) {
	gint32 tradeslot;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if (tradeslot < 0) return;

	currentgame->trade[tradeslot].revision = revision;
}


void trade_update_player(gint32 tradeid, gint32 playerid, gboolean accept)  {

	gint32 tradeslot;
	gint32 i;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if (tradeslot < 0) return;

	for (i = 0; i < TRADE_MAX_PLAYER; i++) {
		trade_player *p = &currentgame->trade[tradeslot].player[i];
		if (!p->player) continue;
		if ((gint32)p->player->playerid != playerid) continue;
		p->accept = accept;
		trade_rebuild_playerlist(tradeslot);
		return;
	}

	/* new participating player */
	for (i = 0; i < TRADE_MAX_PLAYER; i++) {
		trade_player *p = &currentgame->trade[tradeslot].player[i];
		if (p->player) continue;
		p->player = player_from_id(playerid);
		p->accept = accept;
		trade_rebuild_playerlist(tradeslot);
		return;
	}
}


void trade_update_card(gint32 tradeid, gint32 cardid, gint32 targetplayer)  {

	gint32 tradeslot;
	GtkWidget *ProposalList;
	player *from, *to;
	gchar *cardname;
	GtkListStore *store;
	GtkTreeIter iter;
	gboolean valid;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if (tradeslot < 0) return;

	ProposalList = currentgame->trade[tradeslot].ProposalList;
	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ProposalList)));

	/* remove previous cardid */
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
	while (valid) {
		gint32 typeid, card;
		GtkTreeIter curiter = iter;

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter); /* get next iter so we can remove the current entry safely */
		gtk_tree_model_get(GTK_TREE_MODEL(store), &curiter,
		  TRADEPROPOSALLIST_COLUMN_TYPE_ID, &typeid,
		  TRADEPROPOSALLIST_COLUMN_AUX_ID, &card,
		  -1);

		if ( typeid == TRADE_TYPE_CARD  &&  card == cardid ) {
			gtk_list_store_remove(store, &curiter);
		}
	}

	if (targetplayer < 0)  {
		return;
	}

	from = player_from_id(currentgame->card[get_card_slot_with_cardid(cardid)].owner);
	if (!from) {
		return;
	}

	to = player_from_id(targetplayer);
	if (!to) {
		return;
	}

	cardname = g_strdup_printf("card %d: %s", cardid, currentgame->card[ get_card_slot_with_cardid(cardid)  ].title);

	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
	  TRADEPROPOSALLIST_COLUMN_TYPE,"CARD:",
	  TRADEPROPOSALLIST_COLUMN_FROM, from->name,
	  TRADEPROPOSALLIST_COLUMN_GIVES, "gives",
	  TRADEPROPOSALLIST_COLUMN_TO, to->name,
	  TRADEPROPOSALLIST_COLUMN_WHAT, cardname,
	  TRADEPROPOSALLIST_COLUMN_TYPE_ID, TRADE_TYPE_CARD,
	  TRADEPROPOSALLIST_COLUMN_FROM_ID, currentgame->card[ get_card_slot_with_cardid(cardid)  ].owner,
	  TRADEPROPOSALLIST_COLUMN_TO_ID, targetplayer,
	  TRADEPROPOSALLIST_COLUMN_AUX_ID, cardid,
	  -1);

	g_free(cardname);
}


void trade_update_estate(gint32 tradeid, gint32 estateid, gint32 targetplayer)  {

	gint32 tradeslot;
	GtkWidget *ProposalList;
	player *from, *to;
	gchar *estatename;
	GtkListStore *store;
	GtkTreeIter iter;
	gboolean valid;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if (tradeslot < 0) return;

	ProposalList = currentgame->trade[tradeslot].ProposalList;
	estatename = currentgame->estate[estateid].name;
	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ProposalList)));

	/* remove previous estate */
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
	while (valid) {
		gint32 typeid, estate;
		GtkTreeIter curiter = iter;

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter); /* get next iter so we can remove the current entry safely */
		gtk_tree_model_get(GTK_TREE_MODEL(store), &curiter,
		  TRADEPROPOSALLIST_COLUMN_TYPE_ID, &typeid,
		  TRADEPROPOSALLIST_COLUMN_AUX_ID, &estate,
		  -1);

		if ( typeid == TRADE_TYPE_ESTATE  &&  estate == estateid ) {
			gtk_list_store_remove(store, &curiter);
		}
	}

	if (targetplayer < 0) {
		return;
	}

	from = player_from_id(currentgame->estate[estateid].owner);
	if (!from) {
		return;
	}

	to = player_from_id(targetplayer);
	if (!to) {
		return;
	}

	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
	  TRADEPROPOSALLIST_COLUMN_TYPE,"ESTATE:",
	  TRADEPROPOSALLIST_COLUMN_FROM, from->name,
	  TRADEPROPOSALLIST_COLUMN_GIVES, "gives",
	  TRADEPROPOSALLIST_COLUMN_TO, to->name,
	  TRADEPROPOSALLIST_COLUMN_WHAT, estatename,
	  TRADEPROPOSALLIST_COLUMN_TYPE_ID, TRADE_TYPE_ESTATE,
	  TRADEPROPOSALLIST_COLUMN_FROM_ID, currentgame->estate[estateid].owner,
	  TRADEPROPOSALLIST_COLUMN_TO_ID, targetplayer,
	  TRADEPROPOSALLIST_COLUMN_AUX_ID, estateid,
	  -1);
}


void trade_update_money(gint32 tradeid, gint32 playerfrom, gint32 playerto, gint32 money)  {

	gint32 tradeslot;
	GtkWidget *ProposalList;
	player *from, *to;
	gchar *moneystr;
	GtkListStore *store;
	GtkTreeIter iter;
	gboolean valid;

	tradeslot = get_trade_slot_with_tradeid(tradeid);
	if (tradeslot < 0) return;

	ProposalList = currentgame->trade[tradeslot].ProposalList;
	store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ProposalList)));

	/* remove all money proposal if same playerfrom and same playerto */
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
	while (valid) {
		gint32 typeid, fromid, toid;
		GtkTreeIter curiter = iter;

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter); /* get next iter so we can remove the current entry safely */
		gtk_tree_model_get(GTK_TREE_MODEL(store), &curiter,
		  TRADEPROPOSALLIST_COLUMN_TYPE_ID, &typeid,
		  TRADEPROPOSALLIST_COLUMN_FROM_ID, &fromid,
		  TRADEPROPOSALLIST_COLUMN_TO_ID, &toid,
		  -1);

		if ( typeid == TRADE_TYPE_MONEY  &&  fromid == playerfrom  &&  toid == playerto ) {
			gtk_list_store_remove(store, &curiter);
		}
	}

	if (money <= 0) {
		return;
	}

	from = player_from_id(playerfrom);
	if (!from) {
		return;
	}

	to = player_from_id(playerto);
	if (!to) {
		return;
	}

	moneystr = g_strdup_printf("%d money", money);

	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
	  TRADEPROPOSALLIST_COLUMN_TYPE,"MONEY:",
	  TRADEPROPOSALLIST_COLUMN_FROM, from->name,
	  TRADEPROPOSALLIST_COLUMN_GIVES, "gives",
	  TRADEPROPOSALLIST_COLUMN_TO, to->name,
	  TRADEPROPOSALLIST_COLUMN_WHAT, moneystr,
	  TRADEPROPOSALLIST_COLUMN_TYPE_ID, TRADE_TYPE_MONEY,
	  TRADEPROPOSALLIST_COLUMN_FROM_ID, playerfrom,
	  TRADEPROPOSALLIST_COLUMN_TO_ID, playerto,
	  TRADEPROPOSALLIST_COLUMN_AUX_ID, 0,
	  -1);

	g_free(moneystr);
}
