/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef THEME_H
#define THEME_H

#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libxml/parser.h>

#define THEME_FILE_SECTION_GENERAL       10
#define THEME_FILE_SECTION_CARDS         20
#define THEME_FILE_SECTION_TOKEN_GROUP   30
#define THEME_FILE_SECTION_HOUSE_GROUP   40
#define THEME_FILE_SECTION_STAR_GROUP    50
#define THEME_FILE_SECTION_BOARD_GROUP   60
#define THEME_FILE_SECTION_ESTATE_GROUP  70


void     theme_build_database();
gboolean theme_get_valid_slot(guint32 *themeslot);
void     theme_del_entry(guint32 slot);
void     theme_add_entry(gchar *path);
void     theme_build_selection_win();
void     theme_fill_selection_list();
void     theme_preview_free();
void     theme_display(gint32 themeid);
GdkPixbuf *theme_preview_read(gint32 themeid);
void     theme_load(gint32 themeid);

#endif /* THEME_H */
