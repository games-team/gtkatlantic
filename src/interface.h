/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdbool.h>

#define BORDER 3

/* prototypes of interface.c */

void interface_create_mainwindow();
void interface_create_getgamespage(bool autoconnect);
void interface_create_gameconfigpage();
void interface_create_gameinitpage();
void interface_create_gameboardpage();
void interface_gameboard_add_player(player *p);
void interface_gameboard_remove_player(player *p);
gboolean interface_chat_scroll_to_the_end(gpointer data);
void interface_create_chat(GtkWidget *container);
void interface_unparent_chat();
void interface_create_auctionbox(gint32 auctionid, gint32 estateid);
void interface_create_aboutwin();
void interface_create_config_panel();
void interface_create_connectoserver(void);
void interface_create_publicserverlist(void);
void interface_create_nicknamewin();
void interface_create_estates_tree();
void interface_create_helpwin();

void interface_set_infolabel(gchar *text, gchar *color, gboolean eternal);
void interface_create_messagewin(gchar *msg);

void on_board_draw(GtkWidget *widget, cairo_t *cr, gpointer data);
void on_playertoken_draw(GtkWidget *widget, cairo_t *cr, gpointer data);
void on_playercards_draw(GtkWidget *widget, cairo_t *cr, gpointer data);
void update_display();

#endif /* INTERFACE_H */
