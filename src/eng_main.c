/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <glib.h>
#include <string.h>

#include "eng_main.h"

/* Global handler */
_eng *eng;

/* ---- initialise engine */
void eng_init()  {
	guint32 alpha, bright;

#if ENG_DEBUG
	fprintf(stdout, "Initializing engine...\n");
#endif

	eng = g_malloc0( sizeof(_eng) );
	eng->obj = eng_list_new();
	eng->frame = eng_list_new();

	/* init alpha tables */
	eng->alpha = g_malloc( sizeof(eng_alpha) );
	for(alpha = 0 ; alpha < 256 ; alpha++) {
		for(bright = 0 ; bright < 256 ; bright++)  {
			guint8 v = (bright * (alpha+1)) >> 8;  /* alpha+1: 255*256 >> 8 = 255 */
			eng->alpha->fg[alpha][bright] = v;
			eng->alpha->bg[alpha][bright] = bright - v;
		}
	}
}

/* ---- close engine */
void eng_close()  {

	if(!eng) return;

	eng_frame_destroy_all();
	eng_list_destroy(eng->obj);
	eng_list_destroy(eng->frame);
	if(eng->alpha) g_free(eng->alpha);
	g_free(eng);

#if ENG_DEBUG
	fprintf(stdout, "Closing engine...\n");
#endif
}

/* ---- create a new frame */
eng_frame* eng_frame_create()  {

	eng_frame *f;

	f = g_malloc0( sizeof(eng_frame) );
	eng_frame_reset(f);
	f->compute = 1;
	f->obj = eng_list_new();
	f->zone_upd = eng_list_new();
	f->entry = eng_list_append(eng->frame, f);

#if ENG_DEBUG
	fprintf(stdout, "New Frame\n");
#endif

	return(f);
}


/* ---- destroy a frame
 *
 *    close also all pics
 */
void eng_frame_destroy(eng_frame *f)  {

	eng_list_e *lst;

	if(!f)  return;
	eng_list_remove_fast(f->entry);
	while( (lst = eng_list_first(f->obj)))
		eng_pic_free(lst->data);
	eng_frame_reset(f);
	eng_list_destroy(f->obj);
	eng_list_destroy(f->zone_upd);
	g_free(f);

#if ENG_DEBUG
	fprintf(stdout, "Destroy frame\n");
#endif
}


/* ---- destroy all frames */
void eng_frame_destroy_all()  {

	eng_list_e *lst;

	while( (lst = eng_list_first(eng->frame)))
		eng_frame_destroy(lst->data);
}


/* ---- set to 0 all elements in struct */
void eng_frame_reset(eng_frame *f)  {

	if(!f)  return;

	f->compute = 0;
	f->width = 0;
	f->height = 0;
	if(f->memalloc > 0)
		g_free(f->bufout);

	f->memalloc = 0;

	eng_frame_free_zoneupd(f);

	f->num_zone = 0;
	f->x_min = 0;
	f->y_min = 0;
	f->x_max = 0;
	f->y_max = 0;
}


/* ---- free all zone update */
void eng_frame_free_zoneupd(eng_frame *f)  {

	eng_list_e *lst;

	if(!f)  return;
	if(!f->zone_upd)  return;

	for(lst = eng_list_first(f->zone_upd) ; lst ; lst=lst->next)
		g_free(lst->data);
	eng_list_clean(f->zone_upd);
}


/* ---- compute yes this frame (for eng_create_frame_all() ) */
void eng_frame_set_compute(eng_frame *f) {

	if(!f)  return;

	f->compute = 1;
}


/* ---- compute no this frame (for eng_create_frame_all() ) */
void eng_frame_unset_compute(eng_frame *f)  {

	if(!f)  return;

	f->compute = 0;
}


/* ---- clean this frame                              *
 *                                                    *
 *  In the same concept of minimizing memory usage,   *
 *  with this function you can clean all memory       *
 *  used for out buffer.                              *
 *                                                    *
 *  Don't use between frame often drawed,             *
 *  because engine do remake all frame, and not       *
 *  just update this, finally engine can become       *
 *  extremely slow.                                   *
 *                                                    *
 *  This is not dependant with minimizing memory mode *
 *                                                    */
void eng_frame_clean(eng_frame *f)  {

	if(!f)  return;

	if(f->memalloc > 0)
		g_free(f->bufout);

	f->memalloc = 0;

	eng_frame_free_zoneupd(f);
/*
	for(i = 1 ; i <= frame[frame_id]->lastpic ; i++ )  {

		if(engine->active_frame[i])
			frame[frame_id]->obj[i]->change = 1;
	}
*/
}


/* ---- set HEIGHT of a frame */
void eng_frame_set_height(eng_frame *f, guint16 height)  {

	if(!f)  return;

	f->height = height;
}


/* ---- set WIDTH of a frame */
void eng_frame_set_width(eng_frame *f, guint16 width)  {

	if(!f)  return;

	f->width = width;
}

/* ---- put in terminal all arguments of a frame */
void eng_frame_showarg(eng_frame *f)  {

	if(!f)  return;

	fprintf(stdout, "====== FRAME ======\n");
	fprintf(stdout, "  compute      -> %d\n", f->compute);
	fprintf(stdout, "  width        -> %d\n", f->width);
	fprintf(stdout, "  height       -> %d\n", f->height);
	fprintf(stdout, "  ------\n");
	fprintf(stdout, "  memalloc     -> %d bytes\n", f->memalloc);
	fprintf(stdout, "  num_zone     -> %d\n", f->num_zone);
	fprintf(stdout, "====================\n");
}


/* ---- create a new pic */
eng_obj* eng_pic_create(eng_frame *f)  {

	eng_obj *o;

	o = g_malloc0( sizeof(eng_obj) );
	eng_pic_reset(o);
	o->show = 1;
	o->frame = f;

	o->entry_main = eng_list_append(eng->obj, o);
	o->entry = eng_list_append(f->obj, o);

#if ENG_DEBUG
	fprintf(stdout, "New Pic\n");
#endif

	return(o);
}


/* ---- sent a call to destroy pic */
void eng_pic_destroy(eng_obj *o)  {

	if(!o)  return;

	o->destroy = 1;
	o->change = 1;
}


/* ---- destroy pic */
void eng_pic_free(eng_obj *o)  {

	if(!o)  return;

	eng_list_remove_fast(o->entry);
	eng_list_remove_fast(o->entry_main);

	eng_pic_reset(o);
	g_free(o);
	o = NULL;

#if ENG_DEBUG
	fprintf(stdout, "Destroy pic\n");
#endif
}


/* ---- set to 0 all elements in struct */
void eng_pic_reset(eng_obj *o)  {

	if(!o)  return;

	o->show = 0;
	o->destroy = 0;
	o->x = 0;
	o->y = 0;
	o->z = 0;
	o->width = 0;
	o->height = 0;
	o->have_alpha = 0;
	o->have_bgcolor = 0;
	o->alpha = 0;
	o->bgcolor[0] = 0;
	o->bgcolor[1] = 0;
	o->bgcolor[2] = 0;
	o->change = 0;
	o->x_old = 0;
	o->y_old = 0;
	o->height_old = 0;
	o->width_old = 0;
}


/* ---- show this pic */
void eng_pic_show(eng_obj *o) {

	if(!o)  return;

	if(!o->show)  {

		o->show = 1;
		o->change = 1;
	}
}


/* ---- unshow this pic */
void eng_pic_unshow(eng_obj *o) {

	if(!o)  return;

	if(o->show)  {

		o->show = 0;
		o->change = 1;
	}
}


/* ---- redraw this pic */
void eng_pic_redraw(eng_obj *o) {

	if(!o)  return;

	o->change = 1;
}


/* ---- set X pos of pic */
void eng_pic_set_x(eng_obj *o, guint16 x)  {

	if(!o)  return;

	if(x != o->x) {

		o->x = x;
		o->change = 1;
	}
}


/* ---- set Y pos of pic */
void eng_pic_set_y(eng_obj *o, guint16 y)  {

	if(!o)  return;

	if(y != o->y) {

		o->y = y;
		o->change = 1;
	}
}


/* ---- set Z pos of pic */
void eng_pic_set_z(eng_obj *o, guint16 z)  {

	if(!o)  return;

	if(z != o->z) {

		o->z = z;
		o->change = 1;
	}
}


/* ---- set HEIGHT of pic */
void eng_pic_set_height(eng_obj *o, guint16 height)  {

	if(!o)  return;

	if(height != o->height) {

		o->height = height;
		o->change = 1;
	 }
}


/* ---- set WIDTH of pic */
void eng_pic_set_width(eng_obj *o, guint16 width)  {

	if(!o)  return;

	if(width != o->width) {

		o->width = width;
		o->change = 1;
	}
}


/* ---- set ALPHA value */
void eng_pic_set_alpha(eng_obj *o, guint8 alpha)  {

	if(!o)  return;

	if( !o->have_alpha  ||  alpha !=  o->alpha)  {

		if(alpha == 0xff)  {  /* 100% opacity */

			eng_pic_unset_alpha(o);
			return;
		}
		o->alpha = alpha;
		o->have_alpha = 1;
		o->change = 1;
	}
}


/* ---- UNset ALPHA value */
void eng_pic_unset_alpha(eng_obj *o)  {

	if(!o)  return;

	if(o->have_alpha)  {

		o->alpha = 0;
		o->have_alpha = 0;
		o->change = 1;
	}
}


/* ---- set BGCOLOR value */
void eng_pic_set_bgcolor(eng_obj *o, guint32 bgcolor)  {

	guint8 rgb[3];

	if(!o)  return;

	rgb[0] = (bgcolor >> 16) & 0xff; // red
	rgb[1] = (bgcolor >> 8) & 0xff;  // green
	rgb[2] = bgcolor & 0xff;         // blue

	if( !o->have_bgcolor  ||  memcmp(&rgb, o->bgcolor, 3) )  {

		memcpy(o->bgcolor, &rgb, 3);
		o->have_bgcolor = 1;
		o->change = 1;
	}
}


/* ---- UNset BGCOLOR value */
void eng_pic_unset_bgcolor(eng_obj *o)  {

	if(!o)  return;

	if(o->have_bgcolor)  {

		o->bgcolor[0] = 0;
		o->bgcolor[1] = 0;
		o->bgcolor[2] = 0;
		o->have_bgcolor = 0;
		o->change = 1;
	}
}


/* ---- set pic buffer */
void eng_pic_set_pixbuf(eng_obj *o, GdkPixbuf *pixbuff)  {

	if(!o)  return;
	if(!pixbuff)  return;

	o->pixbuf = pixbuff;
	o->change = 1;
}


/* ---- put in terminal all arguments of a pic */
void eng_pic_showarg(eng_obj *o)  {

	if(!o)  return;

	fprintf(stdout, "== PIC ==\n");
	fprintf(stdout, "  show          -> %d\n", o->show);
	fprintf(stdout, "  x             -> %d\n", o->x);
	fprintf(stdout, "  y             -> %d\n", o->y);
	fprintf(stdout, "  z             -> %d\n", o->z);
	fprintf(stdout, "  width         -> %d\n", o->width);
	fprintf(stdout, "  height        -> %d\n", o->height);
	fprintf(stdout, "  have_alpha    -> %d\n", o->have_alpha);
	fprintf(stdout, "  have_bgcolor  -> %d\n", o->have_bgcolor);
	fprintf(stdout, "  alpha         -> %d\n", o->alpha);
	fprintf(stdout, "  bgcolor[R]    -> 0x%.2X\n", o->bgcolor[0]);
	fprintf(stdout, "  bgcolor[G]    -> 0x%.2X\n", o->bgcolor[1]);
	fprintf(stdout, "  bgcolor[B]    -> 0x%.2X\n", o->bgcolor[2]);
	fprintf(stdout, "  ------\n");
	fprintf(stdout, "  change        -> %d\n", o->change);
	fprintf(stdout, "  x old         -> %d\n", o->x_old);
	fprintf(stdout, "  y old         -> %d\n", o->y_old);
	fprintf(stdout, "  width old     -> %d\n", o->width_old);
	fprintf(stdout, "  height old    -> %d\n", o->height_old);
	fprintf(stdout, "  ------\n");
	if(eng_pic_test(o) )
	fprintf(stdout, "  test      -> SUCCESS\n");
	else
	fprintf(stdout, "  test      -> ERROR\n");
	fprintf(stdout, "====================\n");
}


/* ---- test if pic is good */
gboolean eng_pic_test(eng_obj *o)  {

	if(!o)  return(FALSE);

	/* test x & width */
	if(o->x > o->frame->width - o->width) return(FALSE);

	/* test y & height */
	if(o->y > o->frame->height - o->height) return(FALSE);

	return(TRUE);
}
