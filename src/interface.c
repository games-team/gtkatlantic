/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <cairo.h>

#include "global.h"
#include "interface.h"
#include "callback.h"
#include "game.h"
#include "client.h"
#include "display.h"

#include "engine.h"

void interface_create_mainwindow()  {

//	GtkWidget *HandleBar;
		//global->Menu
			GtkWidget *Button;
				GtkWidget *Menu;
					GtkWidget *MenuItem;

	GdkPixbuf *pixbuf;

	global->MainWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(global->MainWin), "GtkAtlantic");
	gtk_widget_set_size_request(global->MainWin, 700, 487);

	/* icons */
	GList *iconlist = NULL;
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_ICON_DIR "/128x128/apps/gtkatlantic.png", NULL);
	if (pixbuf) iconlist = g_list_append(iconlist, pixbuf);
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_ICON_DIR "/64x64/apps/gtkatlantic.png", NULL);
	if (pixbuf) iconlist = g_list_append(iconlist, pixbuf);
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_ICON_DIR "/48x48/apps/gtkatlantic.png", NULL);
	if (pixbuf) iconlist = g_list_append(iconlist, pixbuf);
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_ICON_DIR "/32x32/apps/gtkatlantic.png", NULL);
	if (pixbuf) iconlist = g_list_append(iconlist, pixbuf);
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_ICON_DIR "/16x16/apps/gtkatlantic.png", NULL);
	if (pixbuf) iconlist = g_list_append(iconlist, pixbuf);
	gtk_window_set_default_icon_list(iconlist);

	g_signal_connect(G_OBJECT(global->MainWin), "delete-event", G_CALLBACK(gtk_main_quit), NULL);

	global->MainBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(global->MainWin), global->MainBox);

	global->Menu = gtk_menu_bar_new();
	gtk_box_pack_start(GTK_BOX(global->MainBox), global->Menu, FALSE, FALSE, 0);

	/* GAME MENU */
	Button = gtk_menu_item_new_with_label("Game");
	gtk_menu_shell_append(GTK_MENU_SHELL(global->Menu), Button);

	Menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(Button), Menu);

	MenuItem = gtk_menu_item_new_with_label("Connect to server...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "connect_to_server", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_menu_connect_to_server), NULL);

	MenuItem = gtk_menu_item_new_with_label("Connect to metaserver");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "connect_to_metaserver", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(Callback_ConnectMetaServer), NULL);

	MenuItem = gtk_menu_item_new_with_label("Public server list...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "public_server_list", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_menu_public_server_list), NULL);

	MenuItem = gtk_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Change nickname...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "change_nickname", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_menu_change_nickname), NULL);

	MenuItem = gtk_menu_item_new_with_label("Show estates tree...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "show_estate_tree", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_estates_tree), NULL);

	MenuItem = gtk_menu_item_new_with_label("Add player");
	g_object_set_data(G_OBJECT(global->Menu), "upgrade_spectator", MenuItem);
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	GtkWidget *SubMenu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(MenuItem), SubMenu);

	MenuItem = gtk_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Reconnect (after client crash)");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "reconnect", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_menu_reconnect), NULL);

	MenuItem = gtk_menu_item_new_with_label("Leave game (disconnect)");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_object_set_data(G_OBJECT(global->Menu), "leave_game", MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_menu_LeaveGame), NULL);

	MenuItem = gtk_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Exit");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(gtk_main_quit), NULL);

	/* CONFIG MENU */
	Button = gtk_menu_item_new_with_label("Settings");
	gtk_menu_shell_append(GTK_MENU_SHELL(global->Menu), Button);

	Menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(Button), Menu);

	MenuItem = gtk_menu_item_new_with_label("Preferences...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(Callback_ShowConfiguration), NULL);

//	MenuItem = gtk_menu_item_new_with_label("Themes (unavailable yet)");
//	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
//	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(Callback_menu_Themes), NULL);

	/* HELP MENU */
	Button = gtk_menu_item_new_with_label("Help");
	gtk_menu_shell_append(GTK_MENU_SHELL(global->Menu), Button);

	Menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(Button), Menu);

	MenuItem = gtk_menu_item_new_with_label("Index...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(interface_create_helpwin), NULL);

	MenuItem = gtk_menu_item_new_with_label("About...");
	gtk_menu_shell_append(GTK_MENU_SHELL(Menu), MenuItem);
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(interface_create_aboutwin), NULL);


	global->InfoLabel = gtk_label_new(NULL);
	gtk_widget_set_margin_top(global->InfoLabel, 0);
	gtk_widget_set_margin_bottom(global->InfoLabel, 3);
#if GTK_CHECK_VERSION(3,12,0)
	gtk_widget_set_margin_start(global->InfoLabel, 3);
	gtk_widget_set_margin_end(global->InfoLabel, 3);
#else /* GTK_CHECK_VERSION(3,12,0) */
	gtk_widget_set_margin_left(global->InfoLabel, 3);
	gtk_widget_set_margin_right(global->InfoLabel, 3);
#endif /* GTK_CHECK_VERSION(3,12,0) */
	gtk_widget_set_halign(global->InfoLabel, GTK_ALIGN_START);
	gtk_widget_set_valign(global->InfoLabel, GTK_ALIGN_CENTER);
	gtk_box_pack_end(GTK_BOX(global->MainBox), global->InfoLabel, FALSE, FALSE, 0);

	gtk_widget_show_all(global->MainWin);

	/* Light theme */
	global->bg_green = "#c8ffc8";
	global->bg_red = "#ffc8c8";

	GtkStyleContext *stylecontext = gtk_widget_get_style_context(global->MainWin);
	if (!stylecontext) {
		return;
	}

	GdkRGBA rgba;
	gtk_style_context_get_color(stylecontext, GTK_STATE_FLAG_NORMAL, &rgba);
	if (rgba.red * 0.2126 + rgba.green * 0.7152 + rgba.blue * 0.0722 > 0.5) {
		/* Dark theme */
		global->bg_green = "#007000";
		global->bg_red = "#700000";
	}
}


void interface_create_getgamespage(bool autoconnect)  {

//global->MainBox
	//global->MainVerticalBox
		GtkWidget *GameFrame;
			GtkWidget *GameListVBox;
				GtkWidget *GameScrollwin;
					GtkWidget *GameList;
				GtkWidget *ConnectButton;

	GtkTreeSelection *select;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	global->phase = PHASE_GETGAMES;

	interface_unparent_chat();

	if (global->MainVerticalBox) gtk_widget_destroy(global->MainVerticalBox);
	global->MainVerticalBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
	gtk_container_set_border_width(GTK_CONTAINER(global->MainVerticalBox), 3);
	gtk_box_pack_start(GTK_BOX(global->MainBox), global->MainVerticalBox, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(global->MainVerticalBox), "destroy", G_CALLBACK(CallBack_get_games_page_destroyed), NULL);

	/* list of open game & gametype */
	GameFrame = gtk_frame_new("Create or join a monopd game");
	gtk_box_pack_start(GTK_BOX(global->MainVerticalBox), GameFrame, TRUE, TRUE, 0);

	GameListVBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(GameFrame), GameListVBox);

	/* Game list container */
	GameScrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(GameScrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width(GTK_CONTAINER(GameScrollwin), 2);
	gtk_box_pack_start(GTK_BOX(GameListVBox), GameScrollwin, TRUE, TRUE, 0);

	/* Game list */
	GameList = gtk_tree_view_new();
	gtk_container_add(GTK_CONTAINER(GameScrollwin), GameList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(GameList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(Callback_GetGame_Select), NULL);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Host", renderer, "text", GAMELIST_COLUMN_HOST, "cell-background", GAMELIST_COLUMN_BGCOLOR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(GameList), column);
	column = gtk_tree_view_column_new_with_attributes("Version", renderer, "text", GAMELIST_COLUMN_VERSION, "cell-background", GAMELIST_COLUMN_BGCOLOR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(GameList), column);
	column = gtk_tree_view_column_new_with_attributes("Game", renderer, "text", GAMELIST_COLUMN_GAME, "cell-background", GAMELIST_COLUMN_BGCOLOR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(GameList), column);
	column = gtk_tree_view_column_new_with_attributes("Status", renderer, "text", GAMELIST_COLUMN_STATUS, "cell-background", GAMELIST_COLUMN_BGCOLOR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(GameList), column);
	column = gtk_tree_view_column_new_with_attributes("Players", renderer, "text", GAMELIST_COLUMN_PLAYERS, "cell-background", GAMELIST_COLUMN_BGCOLOR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(GameList), column);

	global->game_store = gtk_list_store_new(GAMELIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT);
	gtk_tree_view_set_model(GTK_TREE_VIEW(GameList), GTK_TREE_MODEL(global->game_store));
	g_object_unref(global->game_store);

	/* Server list */
	global->server_store = gtk_list_store_new(SERVERLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT);

	/* big connect button */
	ConnectButton = gtk_button_new_with_label("Select a game");
	gtk_widget_set_size_request(ConnectButton, 150, -1);
	gtk_container_set_border_width(GTK_CONTAINER(ConnectButton), 3);
	gtk_widget_set_sensitive(ConnectButton, FALSE);
	gtk_box_pack_start(GTK_BOX(GameListVBox), ConnectButton, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(ConnectButton), "clicked", G_CALLBACK(Callback_ConnectButton), NULL);
	g_object_set_data(G_OBJECT(global->MainVerticalBox), "connect_button", ConnectButton);

	GtkWidget *MenuItem;
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_server");
	gtk_widget_set_sensitive(MenuItem, TRUE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_metaserver");
	gtk_widget_set_sensitive(MenuItem, TRUE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "public_server_list");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "change_nickname");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "show_estate_tree");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "upgrade_spectator");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "reconnect");
	gtk_widget_set_sensitive(MenuItem, game_cookie_available());
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "leave_game");
	gtk_widget_set_sensitive(MenuItem, FALSE);

	gtk_widget_show_all(global->MainVerticalBox);

	/* autoconnect to server */
	if (autoconnect) {
		if (config->metaserver_autoconnect) {
			create_connection_metaserver();
		}

		if (config->getgames_autoconnect) {
			create_connection_get_games(config->getgames_host, config->getgames_port);
		}
	}
}


void interface_create_gameconfigpage()  {

//global->MainBox
	//global->MainVerticalBox
		GtkWidget *HorizontalBox;
			GtkWidget *VerticalBoxLeft;
				GtkWidget *ChatFrame;
					//currentgame->ChatBox
			GtkWidget *VerticalBoxRight;
				GtkWidget *PlayerScrollwin;
					GtkWidget *PlayerListFrame;
				GtkWidget *ConfigurationFrame;
					//currentgame->GameConfigBox
				GtkWidget *HorizontalBoxButton;
					GtkWidget *StartButton;
					GtkWidget *LeaveButton;


	GtkTreeSelection *select;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkListStore *store;

	global->phase = PHASE_GAMECREATE;

	interface_unparent_chat();

	gtk_widget_destroy(global->MainVerticalBox);
	global->MainVerticalBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(global->MainVerticalBox), 3);
	gtk_box_pack_start(GTK_BOX(global->MainBox), global->MainVerticalBox, TRUE, TRUE, 0);

	/* main horizontal box */
	HorizontalBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
	gtk_box_pack_start(GTK_BOX(global->MainVerticalBox), HorizontalBox, TRUE, TRUE, 0);

	VerticalBoxLeft = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
	gtk_box_pack_start(GTK_BOX(HorizontalBox), VerticalBoxLeft, TRUE, TRUE, 0);

	/* player list frame */
	PlayerListFrame = gtk_frame_new("Players");
	gtk_widget_set_size_request(PlayerListFrame, -1, 180);
	gtk_box_pack_start(GTK_BOX(VerticalBoxLeft), PlayerListFrame, FALSE, FALSE, 0);

	PlayerScrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_set_border_width(GTK_CONTAINER(PlayerScrollwin), 2);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(PlayerScrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(PlayerListFrame), PlayerScrollwin);

	/* player list */
	currentgame->PlayerList = gtk_tree_view_new();
	gtk_container_add(GTK_CONTAINER(PlayerScrollwin), currentgame->PlayerList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(currentgame->PlayerList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_NONE);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", PLAYERLIST_COLUMN_NAME, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(currentgame->PlayerList), column);
	column = gtk_tree_view_column_new_with_attributes("Host", renderer, "text", PLAYERLIST_COLUMN_HOST, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(currentgame->PlayerList), column);

	store = gtk_list_store_new(PLAYERLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model(GTK_TREE_VIEW(currentgame->PlayerList), GTK_TREE_MODEL(store));
	g_object_unref(store);

	/* chat frame */
	ChatFrame = gtk_frame_new("Chat");
	gtk_box_pack_start(GTK_BOX(VerticalBoxLeft), ChatFrame, TRUE, TRUE, 0);
	interface_create_chat(ChatFrame);


	/* vertical box right */
	VerticalBoxRight = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
	gtk_box_pack_start(GTK_BOX(HorizontalBox), VerticalBoxRight, FALSE, FALSE, 0);

	/* game configuration frame */
	ConfigurationFrame = gtk_frame_new("Game configuration");
	gtk_box_pack_start(GTK_BOX(VerticalBoxRight), ConfigurationFrame, FALSE, FALSE, 0);

	/* config box might already been created by <configupdate/> early events */
	if (!currentgame->GameConfigBox) {
		currentgame->GameConfigBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	}
	gtk_container_set_border_width(GTK_CONTAINER(currentgame->GameConfigBox), 5);
	gtk_container_add(GTK_CONTAINER(ConfigurationFrame), currentgame->GameConfigBox);

	/* horizontal box button */
	HorizontalBoxButton = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
	gtk_box_pack_end(GTK_BOX(VerticalBoxRight), HorizontalBoxButton, FALSE, FALSE, 0);

	/* leave button */
	LeaveButton = gtk_button_new_with_label("Leave game");
	gtk_box_pack_start(GTK_BOX(HorizontalBoxButton), LeaveButton, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(LeaveButton), "clicked", G_CALLBACK(CallBack_menu_LeaveGame), NULL);

	/* start button */
	StartButton = gtk_button_new_with_label("Start game");
	gtk_widget_set_sensitive(StartButton, (global->my_playerid == currentgame->master));
	gtk_box_pack_start(GTK_BOX(HorizontalBoxButton), StartButton, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(StartButton), "clicked", G_CALLBACK(Callback_StartButton), NULL);
	g_object_set_data(G_OBJECT(global->MainVerticalBox), "start_button", StartButton);

	GtkWidget *MenuItem;
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_server");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_metaserver");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "public_server_list");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "change_nickname");
	gtk_widget_set_sensitive(MenuItem, TRUE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "show_estate_tree");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "upgrade_spectator");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "reconnect");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "leave_game");
	gtk_widget_set_sensitive(MenuItem, TRUE);

	gtk_widget_show_all(global->MainVerticalBox);
}


void interface_create_gameinitpage()  {

//global->MainBox
	//global->MainVerticalBox
		GtkWidget *Label;

	global->phase = PHASE_GAMEINIT;

	interface_unparent_chat();

	gtk_widget_destroy(global->MainVerticalBox);
	global->MainVerticalBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(global->MainBox), global->MainVerticalBox, TRUE, TRUE, 0);

	/* text */
	Label = gtk_label_new("Please wait while loading...");
	gtk_widget_set_halign(Label, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_box_pack_start(GTK_BOX(global->MainVerticalBox), Label, TRUE, TRUE, 0);

	gtk_widget_show_all(global->MainVerticalBox);

	/* We are going to receive <display/> events before game is in running state */	
	currentgame->BoardCenter = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	display_init();

	while(gtk_events_pending()) gtk_main_iteration();
}


void interface_create_gameboardpage()  {

//global->MainBox
	//global->MainVerticalBox
		GtkWidget *MainHorizontalBox;
			GtkWidget *VerticalBoxPlayerlistChat;
				//currentgame->PlayerList
					GtkWidget *PlayerSeparator;
				GtkWidget *ChatFrame;
					//call interface_create_chat
			GtkWidget *FixedBoard;
				GtkWidget *EventBox;
					//currentgame->BoardCenter;
					//currentgame->estate[].gdkrgb
						GtkWidget *EstatePopupMenu;

	eng_frame *board_frame;
	eng_obj *pic;
	gint32 i, j, star, houses;
	gchar *text, *tmp1, *tmp2;
	player *p;

	GtkWidget *MenuItem;

	global->phase = PHASE_GAMEPLAY;

	interface_unparent_chat();

	gtk_widget_destroy(global->MainVerticalBox);
	global->MainVerticalBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
	gtk_container_set_border_width(GTK_CONTAINER(global->MainVerticalBox), 3);
	gtk_box_pack_start(GTK_BOX(global->MainBox), global->MainVerticalBox, TRUE, TRUE, 0);

	/* horizontal box for board / player status */
	MainHorizontalBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
	gtk_box_pack_start(GTK_BOX(global->MainVerticalBox), MainHorizontalBox, TRUE, TRUE, 0);

	/* vertical box for player and chat */
	VerticalBoxPlayerlistChat = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
	if(config->game_playerlist_position == GAME_PLAYERLIST_POS_RIGHT)
		gtk_box_pack_end(GTK_BOX(MainHorizontalBox), VerticalBoxPlayerlistChat, TRUE, FALSE, 0);
	else /* left (default) */
		gtk_box_pack_start(GTK_BOX(MainHorizontalBox), VerticalBoxPlayerlistChat, TRUE, TRUE, 0);
	gtk_widget_set_hexpand(VerticalBoxPlayerlistChat, TRUE);
	gtk_widget_set_vexpand(VerticalBoxPlayerlistChat, TRUE);

	/* players */
	currentgame->PlayerList = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(VerticalBoxPlayerlistChat), currentgame->PlayerList, FALSE, FALSE, 0);

	/* first player separator */
	PlayerSeparator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_box_pack_start(GTK_BOX(currentgame->PlayerList), PlayerSeparator, FALSE, FALSE, 0);

	/* chat */
	ChatFrame = gtk_frame_new("Chat");
	gtk_box_pack_start(GTK_BOX(VerticalBoxPlayerlistChat), ChatFrame, TRUE, TRUE, 0);
	interface_create_chat(ChatFrame);

	/* board */
	board_frame = currentgame->board_frame = eng_frame_create();
	eng_frame_set_width(board_frame, data->board_width);
	eng_frame_set_height(board_frame, data->board_height);

	/* add players */
	game_sort_playerlist_by_turnorder();
	for (p = player_h; p; p = p->next) {
		if (p->spectator) continue;
		if (p->game != currentgame->gameid) continue;
		interface_gameboard_add_player(p);
	}
	game_update_tokens();

	FixedBoard = gtk_fixed_new();
	gtk_box_pack_start(GTK_BOX(MainHorizontalBox), FixedBoard, FALSE, FALSE, 0);

	/* board center */
	if (!currentgame->BoardCenter) {
		currentgame->BoardCenter = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
		display_init();
	}
	gtk_container_set_border_width(GTK_CONTAINER(currentgame->BoardCenter), 0);
	gtk_widget_set_size_request( GTK_WIDGET(currentgame->BoardCenter), data->board_center_width, data->board_center_height);
	gtk_fixed_put(GTK_FIXED(FixedBoard), currentgame->BoardCenter, data->board_center_x, data->board_center_y);

	/* board estates */
	for(i = 0 ; i < data->number_estates ; i++)  {

		j = data->estate[i].buffer_board;

		EventBox = gtk_event_box_new();
		gtk_fixed_put(GTK_FIXED(FixedBoard), EventBox, data->estate[i].x, data->estate[i].y);

		currentgame->estate[i].gdkrgb = gtk_drawing_area_new();
		gtk_widget_set_size_request( GTK_WIDGET(currentgame->estate[i].gdkrgb), data->pngfile_board_width[j],  data->pngfile_board_height[j] );

		gtk_widget_set_events(GTK_WIDGET(currentgame->estate[i].gdkrgb), GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

		g_object_set_data(G_OBJECT(EventBox), "estateid", GINT_TO_POINTER(i));
		g_object_set_data(G_OBJECT(currentgame->estate[i].gdkrgb), "estateid", GINT_TO_POINTER(i));

		g_signal_connect_after(G_OBJECT(EventBox), "enter_notify_event", G_CALLBACK(Callback_EnterAnEstate), NULL);
		g_signal_connect(G_OBJECT(EventBox), "leave_notify_event", G_CALLBACK(Callback_LeaveAnEstate), NULL);
		g_signal_connect(G_OBJECT(EventBox), "button_press_event", G_CALLBACK(Callback_ClicOnEstate), NULL);
		g_signal_connect(G_OBJECT(currentgame->estate[i].gdkrgb), "draw", G_CALLBACK(on_board_draw), NULL);

		gtk_container_add(GTK_CONTAINER(EventBox),  currentgame->estate[i].gdkrgb);

		/* building estate popup menu */
		EstatePopupMenu = gtk_menu_new();
		g_object_set_data(G_OBJECT(EstatePopupMenu), "estateid", GINT_TO_POINTER(i));
		g_object_set_data(G_OBJECT(EventBox), "menu", EstatePopupMenu);

		MenuItem = gtk_menu_item_new_with_label("Build house");
		gtk_menu_shell_append(GTK_MENU_SHELL(EstatePopupMenu), MenuItem);
		g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(ESTATE_ACTION_BUILDHOUSE));
		g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuEstate), NULL);
		gtk_widget_show(MenuItem);

		MenuItem = gtk_menu_item_new_with_label("Sell house");
		gtk_menu_shell_append(GTK_MENU_SHELL(EstatePopupMenu), MenuItem);
		g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(ESTATE_ACTION_SELLHOUSE));
		g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuEstate), NULL);
		gtk_widget_show(MenuItem);

		MenuItem = gtk_menu_item_new_with_label("(Un)mortage");
		gtk_menu_shell_append(GTK_MENU_SHELL(EstatePopupMenu), MenuItem);
		g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(ESTATE_ACTION_MORTGAGE));
		g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuEstate), NULL);
		gtk_widget_show(MenuItem);

		MenuItem = gtk_menu_item_new_with_label("Sell property");
		gtk_menu_shell_append(GTK_MENU_SHELL(EstatePopupMenu), MenuItem);
		g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(ESTATE_ACTION_SELL));
		g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuEstate), NULL);
		gtk_widget_show(MenuItem);

		/* set ToolTips */
		text = g_strdup_printf("%s", currentgame->estate[i].name);

		if(currentgame->estate[i].group >= 0)  {
			tmp1 = text;
			tmp2 = g_strdup_printf(" (%s)", currentgame->group[ currentgame->estate[i].group ].name);
			text = g_strconcat(tmp1, tmp2, NULL);
			g_free(tmp1);
			g_free(tmp2);
		}

		if(currentgame->estate[i].price > 0)  {
			tmp1 = text;
			tmp2 = g_strdup_printf("\nPrice: %d", currentgame->estate[i].price);
			text = g_strconcat(tmp1, tmp2, NULL);
			g_free(tmp1);
			g_free(tmp2);
		}

		if(currentgame->estate[i].rent[5] > 0)  {
			tmp1 = text;
			tmp2 = g_strdup_printf(
				"\nNo house: %d, One: %d, Two: %d, Three: %d, Four: %d, Hotel: %d"
				"\nHouse price: %d",
				currentgame->estate[i].rent[0], currentgame->estate[i].rent[1], currentgame->estate[i].rent[2], currentgame->estate[i].rent[3], currentgame->estate[i].rent[4], currentgame->estate[i].rent[5],
				currentgame->estate[i].houseprice   );
			text = g_strconcat(tmp1, tmp2, NULL);
			g_free(tmp1);
			g_free(tmp2);
		}

		if(currentgame->estate[i].mortgageprice > 0)  {
			tmp1 = text;
			tmp2 = g_strdup_printf("\nMortgage value: %d", currentgame->estate[i].mortgageprice);
			text = g_strconcat(tmp1, tmp2, NULL);
			g_free(tmp1);
			g_free(tmp2);
		}

		gtk_widget_set_tooltip_text(EventBox, text);
		g_free(text);

		/* pic for estates / stars / houses */

		/* estate */
		pic = currentgame->estate[i].pic = eng_pic_create(board_frame);
		eng_pic_set_x(pic, data->estate[i].x);
		eng_pic_set_y(pic, data->estate[i].y);
		eng_pic_set_z(pic, 0);
		eng_pic_set_width(pic, data->pngfile_board_width[j]);
		eng_pic_set_height(pic, data->pngfile_board_height[j]);
		eng_pic_set_pixbuf(pic, data->pngfile_board_buf[j]);

		/* star */
		pic = currentgame->estate[i].star_pic = eng_pic_create(board_frame);
		eng_pic_set_x(pic, data->estate[i].xstar);
		eng_pic_set_y(pic, data->estate[i].ystar);
		eng_pic_set_z(pic, 1);

		/* if estate owned  &&  not mortgaged */
		if(currentgame->estate[i].owner > 0  &&  !currentgame->estate[i].mortgaged)  {

			star = player_from_id(currentgame->estate[i].owner)->tokenid;

			pic = currentgame->estate[i].star_pic;
			eng_pic_set_width(pic, data->pngfile_star_width[star]);
			eng_pic_set_height(pic, data->pngfile_star_height[star]);
			eng_pic_set_pixbuf(pic, data->pngfile_star_buf[star]);
			eng_pic_show(pic);
		}

		/* if estate owned  &&  mortgaged */
		else  if(currentgame->estate[i].owner > 0  &&  currentgame->estate[i].mortgaged)  {

			star = player_from_id(currentgame->estate[i].owner )->tokenid;

			pic = currentgame->estate[i].star_pic;
			eng_pic_set_width(pic, data->pngfile_star_m_width[star]);
			eng_pic_set_height(pic, data->pngfile_star_m_height[star]);
			eng_pic_set_pixbuf(pic, data->pngfile_star_m_buf[star]);
			eng_pic_show(pic);
		}

		/* if estate unowned */
		else  /* if( currentgame->estate[i].owner <= 0 ) */ {

			pic = currentgame->estate[i].star_pic;
			eng_pic_unshow(pic);
		}

		/* house image */
		pic = currentgame->estate[i].house_pic = eng_pic_create(board_frame);
		eng_pic_set_x(pic, data->estate[i].xhouse);
		eng_pic_set_y(pic, data->estate[i].yhouse);
		eng_pic_set_z(pic, 1);

		houses = currentgame->estate[i].houses;

		if(houses <= 0)   {

			pic = currentgame->estate[i].house_pic;
			eng_pic_unshow(pic);
		}
		else  if(houses > 0  &&  data->estate[i].type_house == TYPE_HOUSE_HORIZONTAL)  {

			pic = currentgame->estate[i].house_pic;
			eng_pic_set_width(pic, data->pngfile_horiz_house_width[ houses ]);
			eng_pic_set_height(pic, data->pngfile_horiz_house_height[ houses ]);
			eng_pic_set_pixbuf(pic, data->pngfile_horiz_house_buf[ houses ]);
			eng_pic_show(pic);
		}
		else  if(houses > 0  &&  data->estate[i].type_house == TYPE_HOUSE_VERTICAL)  {

			pic = currentgame->estate[i].house_pic;
			eng_pic_set_width(pic, data->pngfile_vert_house_width[ houses ]);
			eng_pic_set_height(pic, data->pngfile_vert_house_height[ houses ]);
			eng_pic_set_pixbuf(pic, data->pngfile_vert_house_buf[ houses ]);
			eng_pic_show(pic);
		}
	}

	/* board first draw */
	eng_create_frame(board_frame);

	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_server");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_metaserver");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "public_server_list");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "change_nickname");
	gtk_widget_set_sensitive(MenuItem, TRUE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "show_estate_tree");
	gtk_widget_set_sensitive(MenuItem, TRUE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "reconnect");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "leave_game");
	gtk_widget_set_sensitive(MenuItem, TRUE);

	gtk_widget_show_all(global->MainVerticalBox);
}


void interface_gameboard_add_player(player *p) {
	GtkWidget *PlayerHorizontalBox;
	GtkWidget *PlayerGrid;
	GtkWidget *PlayerPopupMenu;
	GtkWidget *PlayerSeparator;
	GtkWidget *EventBox;
	GtkWidget *MenuItem;
	gint32 i;
	eng_obj *pic;

	if (global->phase < PHASE_GAMEPLAY) return;
	if (p->spectator) return;
	if (p->game != currentgame->gameid) return;
	if (p->playerlist_LabelNamePlayer) return; /* already created */

	/* select a token which is not already chosen */
	for (i = 0; i <= data->token_max; i++) {
		player *q;
		for (q = player_h; q; q = q->next) {
			if (q->spectator) continue;
			if (q->game != currentgame->gameid) continue;
			if (q->tokenid == i) break;
		}
		if (!q) {
			p->tokenid = i;
			break;
		}
	}
	if (p->tokenid < 0) {
		return;
	}

	p->playerlist_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(currentgame->PlayerList), p->playerlist_box, FALSE, FALSE, 0);

	/* event box */
	EventBox = gtk_event_box_new();
	gtk_widget_set_halign(EventBox, GTK_ALIGN_START);
	gtk_widget_set_valign(EventBox, GTK_ALIGN_CENTER);
	g_signal_connect(G_OBJECT(EventBox), "button_press_event", G_CALLBACK(Callback_ClicOnPlayer), NULL);
	gtk_box_pack_start(GTK_BOX(p->playerlist_box), EventBox, FALSE, FALSE, 0);

	/* box which contain all widgets for a player */
	PlayerHorizontalBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_add(GTK_CONTAINER(EventBox), PlayerHorizontalBox);

	/* building popup player menu */
	PlayerPopupMenu = gtk_menu_new();
	g_object_set_data(G_OBJECT(PlayerPopupMenu), "player", p);
	g_object_set_data(G_OBJECT(EventBox), "menu", PlayerPopupMenu);

	MenuItem = gtk_menu_item_new_with_label("Initiate a trade");
	gtk_menu_shell_append(GTK_MENU_SHELL(PlayerPopupMenu), MenuItem);
	g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(PLAYER_ACTION_TRADE));
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuPlayer), NULL);
	gtk_widget_show(MenuItem);

	MenuItem = gtk_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(PlayerPopupMenu), MenuItem);
	gtk_widget_show(MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Request version");
	gtk_menu_shell_append(GTK_MENU_SHELL(PlayerPopupMenu), MenuItem);
	g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(PLAYER_ACTION_VERSION));
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuPlayer), NULL);
	gtk_widget_show(MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Request date");
	gtk_menu_shell_append(GTK_MENU_SHELL(PlayerPopupMenu), MenuItem);
	g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(PLAYER_ACTION_DATE));
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuPlayer), NULL);
	gtk_widget_show(MenuItem);

	MenuItem = gtk_menu_item_new_with_label("Request ping");
	gtk_menu_shell_append(GTK_MENU_SHELL(PlayerPopupMenu), MenuItem);
	g_object_set_data(G_OBJECT(MenuItem), "action", GINT_TO_POINTER(PLAYER_ACTION_PING));
	g_signal_connect(G_OBJECT(MenuItem), "activate", G_CALLBACK(CallBack_PopupMenuPlayer), NULL);
	gtk_widget_show(MenuItem);

	/* token player in playerlist */
	p->playerlist_token_gdkrgb = gtk_drawing_area_new();
	gtk_widget_set_size_request(p->playerlist_token_gdkrgb, data->playerlist_token_width,  data->playerlist_token_height );
	gtk_widget_set_halign(p->playerlist_token_gdkrgb, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(p->playerlist_token_gdkrgb, GTK_ALIGN_CENTER);
	gtk_box_pack_start(GTK_BOX(PlayerHorizontalBox), p->playerlist_token_gdkrgb, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(p->playerlist_token_gdkrgb), "draw", G_CALLBACK(on_playertoken_draw), NULL);
	g_object_set_data(G_OBJECT(p->playerlist_token_gdkrgb), "player", p);

	/* name, money, cards owned by player */
	PlayerGrid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(PlayerGrid), 0);
	gtk_grid_set_column_spacing(GTK_GRID(PlayerGrid), 0);
	gtk_box_pack_start(GTK_BOX(PlayerHorizontalBox), PlayerGrid, FALSE, FALSE, 0);

	/* cards */
	p->playerlist_cards_gdkrgb = gtk_drawing_area_new();
	gtk_widget_set_size_request( GTK_WIDGET(p->playerlist_cards_gdkrgb), data->playerlist_cards_width,  data->playerlist_cards_height );
	gtk_grid_attach(GTK_GRID(PlayerGrid), p->playerlist_cards_gdkrgb, 0, 1, 2, 1);
	g_signal_connect(G_OBJECT(p->playerlist_cards_gdkrgb), "draw", G_CALLBACK(on_playercards_draw), NULL);
	g_object_set_data(G_OBJECT(p->playerlist_cards_gdkrgb), "player", p);

	/* name */
	p->playerlist_LabelNamePlayer = gtk_label_new(NULL);
	gtk_widget_set_halign(p->playerlist_LabelNamePlayer, GTK_ALIGN_START);
	gtk_widget_set_valign(p->playerlist_LabelNamePlayer, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(PlayerGrid), p->playerlist_LabelNamePlayer, 0, 0, 1, 1);

	if(p->hasturn) {
		gchar *tmp = g_markup_printf_escaped("<span foreground=\"#ff0000\">%s</span>", p->name);
		gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelNamePlayer), tmp);
		g_free(tmp);
	} else {
		gchar *tmp = g_markup_printf_escaped("<span foreground=\"#000000\">%s</span>", p->name);
		gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelNamePlayer), tmp);
		g_free(tmp);
	}

	/* money */
	p->playerlist_LabelMoneyPlayer = gtk_label_new(NULL);
	gtk_widget_set_halign(p->playerlist_LabelMoneyPlayer, GTK_ALIGN_END);
	gtk_widget_set_valign(p->playerlist_LabelMoneyPlayer, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(PlayerGrid), p->playerlist_LabelMoneyPlayer, 1, 0, 1, 1);

	if(p->hasturn) {
		gchar *tmp = g_markup_printf_escaped("<span foreground=\"#ff0000\">%d</span>", p->money);
		gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
		g_free(tmp);
	} else {
		gchar *tmp = g_markup_printf_escaped("<span foreground=\"#000000\">%d</span>", p->money);
		gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
		g_free(tmp);
	}

	/* playerlist token frame */
	eng_frame *token_frame = p->playerlist_token_frame = eng_frame_create();
	eng_frame_set_width(token_frame, data->playerlist_token_width);
	eng_frame_set_height(token_frame, data->playerlist_token_height);

	/* playerlist token pic */
	pic = p->playerlist_token_pic = eng_pic_create(token_frame);
	eng_pic_set_x(pic, (data->playerlist_token_width - data->pngfile_token_width[p->tokenid]) / 2);
	eng_pic_set_y(pic, (data->playerlist_token_height - data->pngfile_token_height[p->tokenid]) / 2);
	eng_pic_set_z(pic, 0);
	eng_pic_set_width(pic, data->pngfile_token_width[p->tokenid]);
	eng_pic_set_height(pic, data->pngfile_token_height[p->tokenid]);
	eng_pic_set_pixbuf(pic, data->pngfile_token_buf[p->tokenid]);

	/* draw token */
	eng_create_frame(token_frame);

	/* engine frame cards */
	eng_frame *card_frame = p->playerlist_cards_frame = eng_frame_create();
	eng_frame_set_width(card_frame, data->playerlist_cards_width);
	eng_frame_set_height(card_frame, data->playerlist_cards_height);

	/* cards */
	for(i = 0; i < data->number_playerlist_card; i++)  {
		gint32 t;

		pic = p->playerlist_cards_pic[i] = eng_pic_create(card_frame);
		eng_pic_set_x(pic, data->playerlist_card[i].x);
		eng_pic_set_y(pic, data->playerlist_card[i].y);
		eng_pic_set_z(pic, data->playerlist_card[i].z);
		t = data->playerlist_card[i].buffer_card;
		eng_pic_set_width(pic, data->pngfile_card_width[t]);
		eng_pic_set_height(pic, data->pngfile_card_height[t]);

		if(currentgame->estate[ data->playerlist_card[i].estateid ].owner  ==  (gint32)p->playerid)  {

			if(currentgame->estate[ data->playerlist_card[i].estateid ].mortgaged)  {

				eng_pic_set_alpha(pic, data->playerlist_cards_alphamortgage);
				eng_pic_set_bgcolor(pic, data->playerlist_cards_cardbgcolormortgage);
			}
			else  {

				eng_pic_set_alpha(pic, data->playerlist_cards_alphaowned);
				eng_pic_set_bgcolor(pic, data->playerlist_cards_cardbgcolor);
			}
		}
		else  {

			eng_pic_set_alpha(pic, data->playerlist_cards_alphaunowned);
			eng_pic_set_bgcolor(pic, data->playerlist_cards_cardbgcolor);
		}

		eng_pic_set_pixbuf(pic, data->pngfile_card_buf[t]);
	}
	/* draw cards */
	eng_create_frame(card_frame);

	/* token on board */
	pic = p->token_pic = eng_pic_create(currentgame->board_frame);
	eng_pic_set_z(pic, 1);
	eng_pic_set_width(pic, data->pngfile_token_width[p->tokenid]);
	eng_pic_set_height(pic, data->pngfile_token_height[p->tokenid]);
	eng_pic_set_pixbuf(pic, data->pngfile_token_buf[p->tokenid]);
	if (p->bankrupt) {
		eng_pic_unshow(pic);
	}

	/* set token location to target location */
	p->location = p->location_to;

	/* separator */
	PlayerSeparator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_box_pack_start(GTK_BOX(p->playerlist_box), PlayerSeparator, FALSE, FALSE, 0);

	gtk_widget_show_all(p->playerlist_box);
}

void interface_gameboard_remove_player(player *p) {
	gint32 i;

	if (global->phase < PHASE_GAMEPLAY) return;
	if (p->spectator) return;

	eng_pic_destroy(p->token_pic);
	p->token_pic = NULL;
	p->tokenid = -1;

	if (p->playerlist_box) {
		gtk_widget_destroy(p->playerlist_box);
		p->playerlist_box = NULL;
		p->playerlist_LabelNamePlayer = NULL;
		p->playerlist_LabelMoneyPlayer = NULL;
		p->playerlist_cards_gdkrgb = NULL;
		p->playerlist_token_gdkrgb = NULL;
	}

	eng_frame_destroy(p->playerlist_token_frame);
	p->playerlist_token_frame = NULL;
	p->playerlist_token_pic = NULL;

	eng_frame_destroy(p->playerlist_cards_frame);
	p->playerlist_cards_frame = NULL;
	for (i = 0; i < MAX_PLAYERLIST_CARDS; i++) {
		p->playerlist_cards_pic[i] = NULL;
	}
}


gboolean interface_chat_scroll_to_the_end(gpointer data) {
	GtkWidget *widget = GTK_WIDGET(data);
	GtkTextBuffer *textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
	GtkTextMark *textmark = gtk_text_buffer_get_mark(textbuff, "endmark");
	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(widget), textmark, 0.0, FALSE, 0.0, 0.0);
	return FALSE;
}


void interface_create_chat(GtkWidget *container)  {

	//currentgame->ChatBox
		GtkWidget *ChatScrolledWindow;
			//currentgame->Chat
		GtkWidget *SendEntry;

	GtkTextBuffer *textbuff;
	GtkTextIter textiter;

	if(! currentgame) return;

	/* already created */
	if(currentgame->ChatBox)  {
		gtk_container_add(GTK_CONTAINER(container), currentgame->ChatBox);
		g_object_unref(G_OBJECT(currentgame->ChatBox));
		/* This is a bit ugly but I could not find a better way */
		g_timeout_add(100, interface_chat_scroll_to_the_end, currentgame->Chat);
		return;
	}

	currentgame->ChatBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_set_size_request(currentgame->ChatBox, -1, 160);
	gtk_container_add(GTK_CONTAINER(container), currentgame->ChatBox);

	/* Create a scrolled window and put the textview in it */
	ChatScrolledWindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ChatScrolledWindow),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(currentgame->ChatBox), ChatScrolledWindow, TRUE, TRUE, 0);

	currentgame->Chat = gtk_text_view_new();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(currentgame->Chat), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(currentgame->Chat), GTK_WRAP_CHAR);
	textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(currentgame->Chat));
	gtk_text_buffer_get_end_iter(textbuff, &textiter);
	gtk_text_buffer_create_mark(textbuff, "endmark", &textiter, FALSE);
	gtk_container_add(GTK_CONTAINER(ChatScrolledWindow), currentgame->Chat);

	/* send entry */
	SendEntry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(currentgame->ChatBox), SendEntry, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(SendEntry), "activate", G_CALLBACK(Callback_SendButton), (gpointer)SendEntry);
}


void interface_unparent_chat()  {

	if(! currentgame) return;
	if(! currentgame->ChatBox) return;

	GtkWidget *parent = gtk_widget_get_parent(currentgame->ChatBox);
	if (parent == NULL) {
		return;
	}

	g_object_ref(G_OBJECT(currentgame->ChatBox));
	gtk_container_remove(GTK_CONTAINER(parent), currentgame->ChatBox);
}


void interface_create_auctionbox(gint32 auctionid, gint32 estateid)  {

	GtkWidget *vbox;
		GtkWidget *PlayerScrollwin;
			GtkWidget *PlayerList;
		GtkWidget *hbox_entry;
			GtkWidget *entry;
		GtkWidget *hbox_buttons;
			GtkWidget *button;

	GtkWidget *label;
	GtkTreeIter iter;
	GtkTreeSelection *select;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkListStore *store;

	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 15);
	g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auctionbox", vbox);
	gtk_box_pack_start(GTK_BOX(currentgame->BoardCenter), vbox, TRUE, TRUE, 0);

	label = gtk_label_new(NULL);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_widget_set_halign(label, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(label, GTK_ALIGN_CENTER);
	gtk_widget_set_margin_bottom(label, 15);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
	gchar *tmp = g_markup_printf_escaped("<span size=\"xx-large\"><b>%s</b></span>", currentgame->estate[estateid].name);
	gtk_label_set_markup(GTK_LABEL(label), tmp);
	g_free(tmp);

	/* player list */
	PlayerScrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(PlayerScrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(vbox), PlayerScrollwin, TRUE, TRUE, 0);

	PlayerList = gtk_tree_view_new();
	gtk_container_add(GTK_CONTAINER(PlayerScrollwin), PlayerList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(PlayerList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_NONE);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Player", renderer, "text", AUCTIONPLAYERLIST_COLUMN_NAME, "weight", AUCTIONPLAYERLIST_WEIGHT, "weight-set", AUCTIONPLAYERLIST_WEIGHTSET, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(PlayerList), column);
	column = gtk_tree_view_column_new_with_attributes("Bid", renderer, "text", AUCTIONPLAYERLIST_COLUMN_BID, "weight", AUCTIONPLAYERLIST_WEIGHT, "weight-set", AUCTIONPLAYERLIST_WEIGHTSET, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(PlayerList), column);

	store = gtk_list_store_new(AUCTIONPLAYERLIST_COLUMN_NUM, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
	g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auction_store", store);
	gtk_tree_view_set_model(GTK_TREE_VIEW(PlayerList), GTK_TREE_MODEL(store));
	g_object_unref(store);

	player *p;
	for (p = player_h; p; p = p->next) {
		if (p->spectator) continue;
		if (p->game != currentgame->gameid) continue;

		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
		AUCTIONPLAYERLIST_COLUMN_NAME, p->name,
		AUCTIONPLAYERLIST_COLUMN_BID, 0,
		AUCTIONPLAYERLIST_COLUMN_PLAYERID, p->playerid,
		AUCTIONPLAYERLIST_WEIGHT, PANGO_WEIGHT_NORMAL,
		AUCTIONPLAYERLIST_WEIGHTSET, TRUE,
		-1);
	}

	/* Status label */
	label = gtk_label_new(NULL);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_set_valign(label, GTK_ALIGN_CENTER);
	gtk_widget_set_margin_top(label, 2);
	gtk_widget_set_margin_bottom(label, 2);
	g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auction_statuslabel", label);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

	/* commands */
	hbox_entry = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

	/* send entry */
	entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox_entry), entry,  FALSE, FALSE, 0);
	gtk_entry_set_width_chars(GTK_ENTRY(entry), 4);
	g_object_set_data(G_OBJECT(entry), "auctionid", GINT_TO_POINTER(auctionid));
	g_signal_connect(G_OBJECT(entry), "activate", G_CALLBACK(Callback_AuctionButton_Absolute), (gpointer)entry);

	button = gtk_button_new_with_label("Bid");
	gtk_box_pack_start(GTK_BOX(hbox_entry), button, FALSE, FALSE, 0);
	g_object_set_data(G_OBJECT(button), "auctionid", GINT_TO_POINTER(auctionid));
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(Callback_AuctionButton_Absolute), (gpointer)entry);

	/* quick button */
	hbox_buttons = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

	button = gtk_button_new_with_label("+1");
	g_object_set_data(G_OBJECT(button), "value_add", GINT_TO_POINTER(1));
	g_object_set_data(G_OBJECT(button), "auctionid", GINT_TO_POINTER(auctionid));
	gtk_box_pack_start(GTK_BOX(hbox_buttons), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(Callback_AuctionButton_Relative), NULL);

	button = gtk_button_new_with_label("+10");
	g_object_set_data(G_OBJECT(button), "value_add", GINT_TO_POINTER(10));
	g_object_set_data(G_OBJECT(button), "auctionid", GINT_TO_POINTER(auctionid));
	gtk_box_pack_start(GTK_BOX(hbox_buttons), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(Callback_AuctionButton_Relative), NULL);

	button = gtk_button_new_with_label("+50");
	g_object_set_data(G_OBJECT(button), "value_add", GINT_TO_POINTER(50));
	g_object_set_data(G_OBJECT(button), "auctionid", GINT_TO_POINTER(auctionid));
	gtk_box_pack_start(GTK_BOX(hbox_buttons), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(Callback_AuctionButton_Relative), NULL);

	button = gtk_button_new_with_label("+100");
	g_object_set_data(G_OBJECT(button), "value_add", GINT_TO_POINTER(100));
	g_object_set_data(G_OBJECT(button), "auctionid", GINT_TO_POINTER(auctionid));
	gtk_box_pack_start(GTK_BOX(hbox_buttons), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(Callback_AuctionButton_Relative), NULL);


	gtk_box_pack_start(GTK_BOX(vbox), hbox_buttons, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox_entry, FALSE, FALSE, 0);

	gtk_widget_show_all(vbox);
}


/* create about window */
void interface_create_aboutwin()
{
	const char *const authors[] = {
		"Sylvain Rochet <gradator@gradator.net>",
		"Xavier Amado <xavier@blackbloodstudios.com>",
		NULL
	};

	gtk_show_about_dialog(GTK_WINDOW(global->MainWin),
		"authors", authors,
		"comments", "irc.freenode.net #gtkatlantic",
		"license-type", GTK_LICENSE_GPL_2_0,
		"logo-icon-name", "gtkatlantic",
		"program-name", "GtkAtlantic",
		"version", VERSION,
		"website", "http://gtkatlantic.gradator.net",
		NULL);
}


/* create config panel */
void interface_create_config_panel()  {

	//global->ConfigWin;
		GtkWidget *vbox;
			GtkWidget *hbox;
				GtkWidget *Frame;
					GtkWidget *hboxFrame;
						GtkWidget *vboxFrame;
							GtkWidget *Label;
							GtkWidget *CButton;
							GtkWidget *Entry;
							GtkWidget *Combo;
							GtkAdjustment *Adjust;
								GtkWidget *Scale;
		GtkWidget *Button;

	gchar *text;

	if(global->ConfigWin)  return;

	global->ConfigWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(global->ConfigWin), "GtkAtlantic: Configuration");
	g_signal_connect(G_OBJECT(global->ConfigWin), "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);
	g_signal_connect(G_OBJECT(global->ConfigWin), "destroy", G_CALLBACK(CallBack_configwindow_destroyed), NULL);

	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), BORDER);
	gtk_container_add(GTK_CONTAINER(global->ConfigWin), vbox);


	/* PLAYER CONFIG (such has nickname) */
	Frame = gtk_frame_new("Player");
	gtk_container_set_border_width(GTK_CONTAINER(Frame), BORDER);
	gtk_box_pack_start(GTK_BOX(vbox), Frame, FALSE, FALSE, 0);

	vboxFrame = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vboxFrame), BORDER);
	gtk_container_add(GTK_CONTAINER(Frame), vboxFrame);

	hboxFrame = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(vboxFrame), hboxFrame, FALSE, FALSE, 0);

	Label = gtk_label_new("Nickname");
	gtk_box_pack_start(GTK_BOX(hboxFrame), Label, FALSE, FALSE, 0);

	Entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hboxFrame), Entry, FALSE, FALSE, 0);
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_player_nickname", Entry);
	gtk_entry_set_text(GTK_ENTRY(Entry), config->nickname);


	/* METASERVER */
	Frame = gtk_frame_new("Meta server");
	gtk_container_set_border_width(GTK_CONTAINER(Frame), BORDER);
	gtk_box_pack_start(GTK_BOX(vbox), Frame, TRUE, TRUE, 0);

	vboxFrame = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vboxFrame), BORDER);
	gtk_container_add(GTK_CONTAINER(Frame), vboxFrame);

	CButton = gtk_check_button_new_with_label("Check new releases of GtkAtlantic");
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_metaserver_sendclientversion", CButton);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(CButton), config->metaserver_sendclientversion);
	gtk_box_pack_start(GTK_BOX(vboxFrame), CButton, FALSE, FALSE, 0);

	CButton = gtk_check_button_new_with_label("Autoconnect");
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_metaserver_autoconnect", CButton);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(CButton), config->metaserver_autoconnect);
	gtk_box_pack_start(GTK_BOX(vboxFrame), CButton, FALSE, FALSE, 0);


	/* DEFAULT CUSTOM SERVER */
	Frame = gtk_frame_new("Default server");
	gtk_container_set_border_width(GTK_CONTAINER(Frame), BORDER);
	gtk_box_pack_start(GTK_BOX(vbox), Frame, TRUE, TRUE, 0);

	vboxFrame = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vboxFrame), BORDER);
	gtk_container_add(GTK_CONTAINER(Frame), vboxFrame);

	hboxFrame = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(vboxFrame), hboxFrame, FALSE, FALSE, 0);

	Label = gtk_label_new("Host");
	gtk_box_pack_start(GTK_BOX(hboxFrame), Label, FALSE, FALSE, 0);

	Entry = gtk_entry_new();
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_customserver_host", Entry);
	gtk_entry_set_text(GTK_ENTRY(Entry), config->getgames_host);
	gtk_box_pack_start(GTK_BOX(hboxFrame), Entry, FALSE, FALSE, 0);

	hboxFrame = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(vboxFrame), hboxFrame, FALSE, FALSE, 0);

	Label = gtk_label_new("Port");
	gtk_box_pack_start(GTK_BOX(hboxFrame), Label, FALSE, FALSE, 0);

	Entry = gtk_entry_new();
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_customserver_port", Entry);
	text = g_strdup_printf("%d", config->getgames_port);
	gtk_entry_set_text(GTK_ENTRY(Entry), text);
	g_free(text);
	gtk_widget_set_size_request(Entry, 45, -1);
	gtk_box_pack_start(GTK_BOX(hboxFrame), Entry, FALSE, FALSE, 0);

	CButton = gtk_check_button_new_with_label("Autoconnect");
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_customserver_autoconnect", CButton);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(CButton), config->getgames_autoconnect);
	gtk_box_pack_start(GTK_BOX(vboxFrame), CButton, FALSE, FALSE, 0);


	/* INTERFACE GAME CONFIG */
	Frame = gtk_frame_new("Game interface");
	gtk_container_set_border_width(GTK_CONTAINER(Frame), BORDER);
	gtk_box_pack_start(GTK_BOX(vbox), Frame, FALSE, FALSE, 0);

	vboxFrame = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vboxFrame), BORDER);
	gtk_container_add(GTK_CONTAINER(Frame), vboxFrame);

	hboxFrame = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(vboxFrame), hboxFrame, FALSE, FALSE, 0);

	Label = gtk_label_new("Display player list at ");
	gtk_box_pack_start(GTK_BOX(hboxFrame), Label, FALSE, FALSE, 0);

	Combo = gtk_combo_box_text_new();
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_gameinterface_playerlistposition", Combo);
	gtk_widget_set_size_request(Combo, 60, -1);
	gtk_box_pack_start(GTK_BOX(hboxFrame), Combo, FALSE, FALSE, 0);

	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), "left");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(Combo), "right");

	if(config->game_playerlist_position == GAME_PLAYERLIST_POS_LEFT)
		gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 0);
	else
		gtk_combo_box_set_active(GTK_COMBO_BOX(Combo), 1);


	hboxFrame = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, BORDER);
	gtk_box_pack_start(GTK_BOX(vboxFrame), hboxFrame, FALSE, FALSE, 0);

	Label = gtk_label_new("Token speed (in ms per estate) : ");
	gtk_box_pack_start(GTK_BOX(hboxFrame), Label, FALSE, FALSE, 0);

	Adjust = gtk_adjustment_new (config->game_token_animation_speed, 25, 500, 25, 25, 0);
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_gameinterface_tokenanimatespeed", Adjust);
	Scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, GTK_ADJUSTMENT(Adjust));
	gtk_widget_set_size_request(Scale, 200, -1);
	gtk_scale_set_value_pos (GTK_SCALE (Scale), GTK_POS_TOP);
	gtk_scale_set_digits (GTK_SCALE (Scale), 0);
	gtk_box_pack_start (GTK_BOX (hboxFrame), Scale, FALSE, FALSE, 0);

	CButton = gtk_check_button_new_with_label("Show in transparency the tokens of players who are not moving now");
	g_object_set_data(G_OBJECT(global->ConfigWin), "config_gameinterface_tokentransparency", CButton);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(CButton), config->game_token_transparency);
	gtk_box_pack_start(GTK_BOX(vboxFrame), CButton, FALSE, FALSE, 0);

	/* CANCEL/OK BUTTON */
	/* horizontal box buttons */
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	Button = gtk_button_new_with_label("CANCEL");
	gtk_widget_set_size_request(Button, 130, 45);
	gtk_container_set_border_width(GTK_CONTAINER(Button), 10);
	gtk_box_pack_start(GTK_BOX(hbox), Button, TRUE, FALSE, 0);
	g_signal_connect_object(G_OBJECT(Button), "clicked", G_CALLBACK(gtk_widget_destroy), global->ConfigWin, G_CONNECT_SWAPPED);

	Button = gtk_button_new_with_label("OK");
	gtk_widget_set_size_request(Button, 130, 45);
	gtk_container_set_border_width(GTK_CONTAINER(Button), 10);
	gtk_box_pack_start(GTK_BOX(hbox), Button, TRUE, FALSE, 0);
	g_signal_connect(G_OBJECT(Button), "clicked", G_CALLBACK(CallBack_ok_button_configwindow), NULL);

	gtk_widget_show_all(global->ConfigWin);
}


/* create the little window connect to server */
void interface_create_connectoserver(void) {

	GtkWidget *win;
		GtkWidget *grid;
			GtkWidget *Entry;
			GtkWidget *Label;

	gchar *text;

	win = gtk_dialog_new_with_buttons("GtkAtlantic: Connect to server",
			GTK_WINDOW(global->MainWin), GTK_DIALOG_MODAL,
			"Close", GTK_RESPONSE_CLOSE, "Connect", GTK_RESPONSE_OK, NULL);

	grid = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(grid), 5);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
	gtk_container_set_border_width(GTK_CONTAINER(grid), 10);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(win))), grid);

	Label = gtk_label_new("Hostname:");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(grid), Label, 0, 0, 1, 1);

	Entry = gtk_entry_new();
	gtk_widget_set_size_request(Entry, 150, -1);
	g_object_set_data(G_OBJECT(win), "host", Entry);
	gtk_entry_set_text(GTK_ENTRY(Entry), config->getgames_host);
	gtk_grid_attach(GTK_GRID(grid), Entry, 1, 0, 1, 1);

	Label = gtk_label_new("Port:");
	gtk_widget_set_halign(Label, GTK_ALIGN_END);
	gtk_widget_set_valign(Label, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(grid), Label, 0, 1, 1, 1);

	Entry = gtk_entry_new();
	gtk_widget_set_size_request(Entry, 150, -1);
	g_object_set_data(G_OBJECT(win), "port", Entry);
	text = g_strdup_printf("%d", config->getgames_port);
	gtk_entry_set_text(GTK_ENTRY(Entry), text);
	g_free(text);
	gtk_grid_attach(GTK_GRID(grid), Entry, 1, 1, 1, 1);

	g_signal_connect(G_OBJECT(win), "response", G_CALLBACK(CallBack_response_connect_to_server_window), win);

	gtk_widget_show_all(win);
}


/* create the public server list window */
void interface_create_publicserverlist(void) {

	GtkWidget *win;
		GtkWidget *frame;
			GtkWidget *scrollwin;
				GtkWidget *ServerList;

	GtkCellRenderer *renderer;
	GtkTreeSelection *select;
	GtkTreeViewColumn *column;

	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(win, 700, 300);
	gtk_window_set_title(GTK_WINDOW(win), "GtkAtlantic: Public server list");
	g_signal_connect(G_OBJECT(win), "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);

	frame = gtk_frame_new("Public server list");
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
	gtk_container_add(GTK_CONTAINER(win), frame);

	scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width(GTK_CONTAINER(scrollwin), 5);
	gtk_container_add(GTK_CONTAINER(frame), scrollwin);

	ServerList = gtk_tree_view_new();
	gtk_container_add(GTK_CONTAINER(scrollwin), ServerList);
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(ServerList));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_NONE);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Host", renderer, "text", SERVERLIST_COLUMN_HOST, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ServerList), column);
	column = gtk_tree_view_column_new_with_attributes("Port", renderer, "text", SERVERLIST_COLUMN_PORT, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ServerList), column);
	column = gtk_tree_view_column_new_with_attributes("Version", renderer, "text", SERVERLIST_COLUMN_VERSION, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ServerList), column);
	column = gtk_tree_view_column_new_with_attributes("Users", renderer, "text", SERVERLIST_COLUMN_USERS, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(ServerList), column);

	gtk_tree_view_set_model(GTK_TREE_VIEW(ServerList), GTK_TREE_MODEL(global->server_store));

	gtk_widget_show_all(win);
}


/* create the little window change nickname */
void interface_create_nicknamewin()  {

	GtkWidget *NicknameWin;
		GtkWidget *HBox;
			GtkWidget *Entry;
			GtkWidget *Label;

	NicknameWin = gtk_dialog_new_with_buttons("GtkAtlantic: Nickname",
			GTK_WINDOW(global->MainWin), GTK_DIALOG_MODAL,
			"_OK", GTK_RESPONSE_OK, NULL);

	HBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(HBox), 10);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(NicknameWin))), HBox);

	Label = gtk_label_new("Nickname:");
	gtk_box_pack_start(GTK_BOX(HBox), Label, FALSE, FALSE, 5);

	Entry = gtk_entry_new();
	gtk_widget_set_size_request(Entry, 150, -1);
	g_object_set_data(G_OBJECT(NicknameWin), "nickname", Entry);
	gtk_entry_set_text(GTK_ENTRY(Entry), player_from_id(global->my_playerid)->name);
	gtk_box_pack_start(GTK_BOX(HBox), Entry, TRUE, TRUE, 0);

	g_signal_connect(G_OBJECT(NicknameWin), "response", G_CALLBACK(CallBack_response_changenick_window), NicknameWin);

	gtk_widget_show_all(NicknameWin);
}


/* create estates tree */
void interface_create_estates_tree()  {

	GtkWidget *EstateWin;
		GtkWidget *HBox;
			GtkWidget *ScrollWin;
				GtkWidget *EstateTree;
			GtkWidget *VBox;
				GtkWidget *Label;

	GtkTreeViewColumn *estateTreeCol;
	GtkCellRenderer *estateTreeRenderer;
	GtkTreeStore *stateTreestore;
	GtkTreeIter stateTreeIterGroup[MAX_GROUPS], stateTreeIterOther, stateTreeIterChild;
	GtkTreeSelection *select;
	guint32 i;

	if(currentgame->WinEstateTree)  return;

	currentgame->WinEstateTree = EstateWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(EstateWin, 450, 400);
	gtk_window_set_title(GTK_WINDOW(EstateWin), "GtkAtlantic: Estates Tree");
	g_signal_connect(G_OBJECT(EstateWin), "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);
	g_signal_connect(G_OBJECT(EstateWin), "destroy", G_CALLBACK(CallBack_EstateTree_Destroyed), NULL);

	HBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	g_object_set_data(G_OBJECT(currentgame->WinEstateTree), "parent_box", HBox);
//	gtk_container_set_border_width(GTK_CONTAINER(HBox), 10);
	gtk_container_add(GTK_CONTAINER(EstateWin), HBox);

	/* ScrollWin win */
	ScrollWin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ScrollWin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_set_size_request(ScrollWin, 250, -1);
	gtk_box_pack_start(GTK_BOX(HBox), ScrollWin, FALSE, TRUE, 0);

	/* estate tree */
	EstateTree = gtk_tree_view_new();
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(EstateTree));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(CallBack_EstateTree_SelectRow), NULL);

	gtk_container_add(GTK_CONTAINER(ScrollWin), EstateTree);
	
	estateTreeCol = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(estateTreeCol, "Estates");
	gtk_tree_view_append_column(GTK_TREE_VIEW(EstateTree), estateTreeCol);
	
	estateTreeRenderer = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(estateTreeCol, estateTreeRenderer, TRUE);
	gtk_tree_view_column_add_attribute(estateTreeCol, estateTreeRenderer, "text", 0);


	stateTreestore = gtk_tree_store_new(2, G_TYPE_STRING, G_TYPE_INT);

	/* create groups node */
	for(i = 0 ; i < MAX_GROUPS ; i++)  {
		if(! currentgame->group[i].name)  continue;

		gtk_tree_store_append(stateTreestore, &stateTreeIterGroup[i], NULL);
		gtk_tree_store_set(stateTreestore, &stateTreeIterGroup[i], 0, currentgame->group[i].name, 1, -1, -1);
	}
	gtk_tree_store_append(stateTreestore, &stateTreeIterOther, NULL);
	gtk_tree_store_set(stateTreestore, &stateTreeIterOther, 0, "Other", 1, -1, -1);

	/* create estates node */
	for(i = 0 ; i < data->number_estates ; i++)  {
		if(currentgame->estate[i].group >= 0) {
			gtk_tree_store_append(stateTreestore, &stateTreeIterChild, &stateTreeIterGroup[currentgame->estate[i].group]);
		} else {
			gtk_tree_store_append(stateTreestore, &stateTreeIterChild, &stateTreeIterOther);
		}
		gtk_tree_store_set(stateTreestore, &stateTreeIterChild, 0, currentgame->estate[i].name, 1, i, -1);
	}

	gtk_tree_view_set_model(GTK_TREE_VIEW(EstateTree), GTK_TREE_MODEL(stateTreestore));
	g_object_unref(stateTreestore); /* the TreeView creates its own reference */

	/* Expand groups */
	for(i = 0 ; i < MAX_GROUPS ; i++)  {
		GtkTreePath *path;

		if(! currentgame->group[i].name)  continue;

		path = gtk_tree_model_get_path(GTK_TREE_MODEL(stateTreestore), &stateTreeIterGroup[i]);
		if(path) {
			gtk_tree_view_expand_row(GTK_TREE_VIEW(EstateTree), path, FALSE);
			gtk_tree_path_free(path);
		}
	}

	/* info box */
	VBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	g_object_set_data(G_OBJECT(currentgame->WinEstateTree), "info_box", VBox);
	gtk_container_set_border_width(GTK_CONTAINER(VBox), 10);
	gtk_box_pack_start(GTK_BOX(HBox), VBox, TRUE, TRUE, 0);

	/* default text */
	Label = gtk_label_new("Select an estate...");
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_START);
	gtk_box_pack_start(GTK_BOX(VBox), Label, FALSE, TRUE, 0);


	gtk_widget_show_all(EstateWin);
}


/* create help window */
void interface_create_helpwin()  {

	GtkWidget *HelpWin;
		GtkWidget *Scrolled;
			GtkWidget *label;

	if(global->HelpWin)  return;

	global->HelpWin = HelpWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(HelpWin, 700, 500);
	gtk_window_set_title(GTK_WINDOW(HelpWin), "GtkAtlantic: Help");
	g_signal_connect(G_OBJECT(HelpWin), "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);
	g_signal_connect(G_OBJECT(HelpWin), "destroy", G_CALLBACK(CallBack_HelpWin_Destroyed), NULL);

	Scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Scrolled), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(global->HelpWin), Scrolled);

	label = gtk_label_new(NULL);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_container_add(GTK_CONTAINER(Scrolled), label);

	gchar *tmp = g_markup_printf_escaped(
	  "<span size=\"xx-large\"><b>GtkAtlantic Help</b></span>\n"
	  "\n"
	  "<span size=\"x-large\"><b>Contents</b></span>\n"
	  "\n"
	  "1) About Atlantic\n"
	  "    i. What is the Atlantic game?\n"
	  "    ii. What is GtkAtlantic?\n"
	  "2) How to play\n"
	  "3) Fun chat macros\n"
	  "    i. Remote Macros\n"
	  "    ii. Local Macros\n"
	  "\n"
	  "\n"
	  "<span size=\"x-large\"><b>1) About Atlantic</b></span>\n"
	  "\n"
	  "<span size=\"large\"><b>i. What is the Atlantic game?</b></span>\n"
	  "\n"
	  "The goal in playing Atlantic is to <i>Get rich quick and win!</i> The richest player is the winner :)\n"
	  "\n"
	  "In order to win, you need to buy properties quickly and profitably. To succeed, you need at least one set of the same group of "
	  "properties, so that you can build Houses and ultimately Hotels upon them.\n"
	  "\n"
	  "Play begins from <i>Start</i> square. Players move their tokens around the board by clicking the <i>Roll Dice</i> button. When a player lands "
	  "on an unowned estate, he may <i>Buy</i> it from the bank or put it up for <i>Auction</i>. When a player lands on a estate owned by custom "
	  "player, he must pay rent to the owner.\n"
	  "\n"
	  "Building houses or hotels dramatically increase payable rents - so much so that it may force other players to declare 'Bankruptcy'. "
	  "To raise money, you can mortgage any owned estate to the bank.\n"
	  "\n"
	  "The <i>Chance</i> and <i>Community</i> squares give you a card whose instructions must be followed. Sometimes you are sent to Jail! :(\n"
	  "\n"
	  "Atlantic is a game that combines subtlety and a sense of business - a good mood helps if you are losing! :)\n"
	  "\n"
	  "<span size=\"large\"><b>ii. What is GtkAtlantic?</b></span>\n"
	  "\n"
	  "GtkAtlantic is a network client to play the Atlantic game on monopd servers. The Atlantic game looks like a Monopoly® game.\n"
	  "\n"
	  "\n"
	  "<span size=\"x-large\"><b>2) How to Play</b></span>\n"
	  "\n"
	  "Well, it is quite easy to play with GtkAtlantic. If you know how to play Monopoly® you can probably play without problem.\n"
	  "\n"
	  "<b>Roll dice:</b> Clic the <i>Roll</i> button.\n"
	  "\n"
	  "<b>Buy a house:</b> Clic on the estate on which you want to build a house then choose <i>Build house</i>.\n"
	  "<b>Sell a house:</b> Clic on the estate on which you want to sell a house then choose <i>Sell house</i>.\n"
	  "<b>Mortgage an estate:</b> Clic on the estate you want to mortgage then choose <i>(Un)mortgage</i>.\n"
	  "\n"
	  "<b>Start a trade with a player:</b> Clic on the player you want to trade with, then select <i>Initiate a trade</i>.\n"
	  "\n"
	  "\n"
	  "<span size=\"x-large\"><b>3) Fun chat commands</b></span>\n"
	  "\n"
	  "<span size=\"large\"><b>i. Remote commands</b></span>\n"
	  "\n"
	  "Note that all players will know what you are doing, and all players will receive what you have requested. "
	  "Please do not flood chat - use with care because some players don't like receiving this information. "
	  "Only a GtkAtlantic client will answer these requests.\n"
	  "\n"
	  "<b>!version [player]</b>\n"
	  "Returns player client version. If no player is specified, returns client version of all players.\n"
	  "\n"
	  "<b>!date [player]</b>\n"
	  "Returns player local time. If no player is specified, returns date of all players.\n"
	  "\n"
	  "<b>!ping [player]</b>\n"
	  "Returns pong if player is still connected. If no player is specified, returns pong of all players\n"
	  "\n"
	  "<span size=\"large\"><b>ii. Local commands</b></span>\n"
	  "\n"
	  "Local commands to use whenever you want and as often as you need.\n"
	  "\n"
	  "<b>/me msg</b>\n"
	  "Sends an action message\n"
	  "\n"
	  "<b>/date</b>\n"
	  "Returns local time\n"
	  "\n"
	  "<b>/nick [new nick]</b>\n"
	  "Changes nick\n"
	  "\n"
	  "<b>/assets [player]</b>\n"
	  "Returns player assets. If no player is specified, returns assets of all players.\n"
	  "\n"
	  "<b>/elapsed</b>\n"
	  "Returns elapsed time since game start.\n"
	  "\n"
	  );
	gtk_label_set_markup(GTK_LABEL(label), tmp);
	g_free(tmp);

	gtk_widget_show_all(HelpWin);
}


static gboolean infolabel_timeout(gpointer data) {
	(void)data;

	gtk_label_set_text(GTK_LABEL(global->InfoLabel), NULL);
	global->timeout_InfoLabel = 0;
	return FALSE;
}

void interface_set_infolabel(gchar *text, gchar *color, gboolean eternal) {
	gchar *tmp = g_markup_printf_escaped("<span foreground=\"#%s\">%s</span>", color, text);
	gtk_label_set_markup(GTK_LABEL(global->InfoLabel), tmp);
	g_free(tmp);
	if (global->timeout_InfoLabel) {
		g_source_remove(global->timeout_InfoLabel);
		global->timeout_InfoLabel = 0;
	}
	if (!eternal) {
		global->timeout_InfoLabel = g_timeout_add_seconds(5, infolabel_timeout, NULL);
	}
}


/* generic window with a label into */
void interface_create_messagewin(gchar *msg)  {
	(void)msg;
#if 0
	GtkWidget *MessageWin;
		GtkWidget *VBox;
			GtkWidget *Label;
			GtkWidget *Button;


	MessageWin = gtk_window_new(GTK_WINDOW_DIALOG);
	gtk_window_set_title(GTK_WINDOW(MessageWin), "GtkAtlantic: Message");
	gtk_window_set_position(GTK_WINDOW(MessageWin), GTK_WIN_POS_CENTER);
	gtk_window_set_transient_for(GTK_WINDOW(MessageWin), GTK_WINDOW(global->MainWin));
	gtk_window_set_modal(GTK_WINDOW(MessageWin), TRUE);
	gtk_widget_set_size_request(MessageWin, -1, -1);
	g_signal_connect(G_OBJECT(MessageWin), "delete_event", G_CALLBACK(gtk_widget_destroy), NULL);

	VBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_set_border_width(GTK_CONTAINER(VBox), 10);
	gtk_container_add(GTK_CONTAINER(MessageWin), VBox);

	/* text  */
	Label = gtk_label_new(msg);
	gtk_label_set_line_wrap(GTK_LABEL(Label), TRUE);
	gtk_box_pack_start(GTK_BOX(VBox), Label, TRUE, TRUE, 0);

	/* ok button */
	Button = gtk_button_new_with_label("OK");
	gtk_widget_set_halign(Button, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(Button, GTK_ALIGN_CENTER);
	gtk_widget_set_size_request(Button, 70, -1);
	gtk_box_pack_start(GTK_BOX(VBox), Button, FALSE, FALSE, 0);

	g_signal_connect_object(G_OBJECT(Button), "clicked", G_CALLBACK(gtk_widget_destroy), MessageWin, G_CONNECT_SWAPPED);

	gtk_widget_show_all(MessageWin);
#endif
}



/* when receive event: redraw an estate */
void on_board_draw(GtkWidget *widget, cairo_t *cr, gpointer d) {
	(void)d;
	gint32 id;
	eng_frame *frame;
	gint32 width, height, stride;
	eng_coord coord;
	cairo_surface_t *surface;
	guint8 *buf;
	gint32 bufferid;

	id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "estateid"));
 	frame = currentgame->board_frame;
	bufferid = data->estate[id].buffer_board;

	width = data->pngfile_board_width[bufferid];
	height = data->pngfile_board_height[bufferid];
	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	coord.x = data->estate[id].x;
	coord.y = data->estate[id].y;
	coord.width = width;
	coord.height = height;
	coord.stride = stride;

	buf = eng_get_zone(frame, &coord);
	surface = cairo_image_surface_create_for_data((unsigned char*)buf, CAIRO_FORMAT_ARGB32, width, height, stride);
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	cairo_surface_destroy(surface);
	g_free(buf);
}

/* when receive event: redraw player token */
void on_playertoken_draw(GtkWidget *widget, cairo_t *cr, gpointer d) {
	(void)d;
	eng_frame *frame;
	gint32 width, height, stride;
	eng_coord coord;
	cairo_surface_t *surface;
	guint8 *buf;

	player *p = g_object_get_data(G_OBJECT(widget), "player");
	frame = p->playerlist_token_frame;

	width = data->playerlist_token_width;
	height = data->playerlist_token_height;
	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	coord.x = 0;
	coord.y = 0;
	coord.width = width;
	coord.height = height;
	coord.stride = stride;

	buf = eng_get_zone(frame, &coord);
	surface = cairo_image_surface_create_for_data((unsigned char*)buf, CAIRO_FORMAT_ARGB32, width, height, stride);
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	cairo_surface_destroy(surface);
	g_free(buf);
}

/* when receive event: redraw player cards */
void on_playercards_draw(GtkWidget *widget, cairo_t *cr, gpointer d) {
	(void)d;
	eng_frame *frame;
	gint32 width, height, stride;
	eng_coord coord;
	cairo_surface_t *surface;
	guint8 *buf;

	player *p = g_object_get_data(G_OBJECT(widget), "player");
	frame = p->playerlist_cards_frame;

	width = data->playerlist_cards_width;
	height = data->playerlist_cards_height;
	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	coord.x = 0;
	coord.y = 0;
	coord.width = width;
	coord.height = height;
	coord.stride = stride;

	buf = eng_get_zone(frame, &coord);
	surface = cairo_image_surface_create_for_data((unsigned char*)buf, CAIRO_FORMAT_ARGB32, width, height, stride);
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	cairo_surface_destroy(surface);
	g_free(buf);
}


/* update display */
void update_display() {
	guint32 i;
	eng_frame *frame;

	if (global->phase != PHASE_GAMEPLAY) {
		return;
	}

	/* draw the board */
	frame = currentgame->board_frame;
	eng_create_frame(frame);

	for(i = 0 ; i < data->number_estates ; i++)  {
		eng_coord coord;
		gint32 width, height;
		gint32 bufferid;

		if(! currentgame->estate[i].gdkrgb) continue;
		if(! frame) continue;

		bufferid = data->estate[i].buffer_board;

		width = data->pngfile_board_width[bufferid];
		height = data->pngfile_board_height[bufferid];
		coord.x1 = data->estate[i].x;
		coord.y1 = data->estate[i].y;
		coord.x2 = coord.x1 + width -1;
		coord.y2 = coord.y1 + height -1;

		/* force draw event */
		if (eng_zone_is_modified(frame, &coord) ) {
			gtk_widget_queue_draw_area(currentgame->estate[i].gdkrgb, 0, 0, width, height);
		}
	}


	/* draw playerlist */
	player *p;
	for (p = player_h; p; p = p->next) {
		eng_frame *frame;
		gint32 width, height;
		eng_coord coord;

		if (p->spectator) continue;
		if (p->game != currentgame->gameid)  continue;
		if (!p->playerlist_cards_gdkrgb) continue;

		/* playerlist cards */
		frame = p->playerlist_cards_frame;
		eng_create_frame(frame);

		width = data->playerlist_cards_width;
		height = data->playerlist_cards_height;
		coord.x1 = 0;
		coord.y1 = 0;
		coord.x2 = width -1;
		coord.y2 = height -1;

		/* force draw event */
		if ( eng_zone_is_modified(frame, &coord) ) {
			gtk_widget_queue_draw_area(p->playerlist_cards_gdkrgb, 0, 0, width, height);
		}

		/* playerlist token */
		frame = p->playerlist_token_frame;
		eng_create_frame(frame);

		width = data->playerlist_token_width;
		height = data->playerlist_token_height;
		coord.x1 = 0;
		coord.y1 = 0;
		coord.x2 = width -1;
		coord.y2 = height -1;

		/* force draw event */
		if ( eng_zone_is_modified(frame, &coord) ) {
			gtk_widget_queue_draw_area(p->playerlist_token_gdkrgb, 0, 0, width, height);
		}
	}
}
