/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef GAME_H
#define GAME_H

#include "global.h"

void create_home_directory(void);
void read_config_files();
void create_connection_metaserver(void);
void create_connection_get_games(gchar *host, gint32 port);
void text_insert_chat(gchar* text, gint32 lenght);
game *game_new(gint32 id);
void game_free(gint32 id);
game *game_find(gint32 id);
void game_quit();
player *game_new_player(guint32 id);
player *player_from_id(gint32 playerid);
player *player_from_name(gchar *name);
gint32 get_playerlistcard_id_with_estate(gint32 estate);
gboolean game_get_valid_command_slot(gint32 *commandslot);
gint32 get_command_button_slot_with_command(gchar *command);
void parse_specific_chat_message(gchar *message);
void parse_specific_send_message(gchar *message);
void game_sort_playerlist_by_playerid(void);
void game_sort_playerlist_by_turnorder(void);
void game_switch_status();
void game_write_cookie();
void game_delete_cookie(void);
gboolean game_cookie_available(void);
gboolean game_get_valid_card_slot(gint32 *cardslot);
gint32 get_card_slot_with_cardid(gint32 cardid);
gint32 game_nb_players();
gboolean game_get_valid_trade_slot(gint32 *tradeslot);
gint32 get_trade_slot_with_tradeid(gint32 tradeid);
gint32 get_estateid_by_estatename(gchar *name);
void game_buildplayerlist();
gint32 game_get_assets_player(gint32 playerid);
void game_update_tokens();
void game_initiate_token_movement();
gboolean game_move_tokens(gpointer data);
void game_free_player(player *p);
void game_exit(void);

#endif /* GAME_H */
